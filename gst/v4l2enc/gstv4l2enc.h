/*
* Copyright (C) 2013 STMicroelectronics SA
*
* Author: <yannick.fertre@st.com> for STMicroelectronics.
* Author: <hugues.fruchet@st.com> for STMicroelectronics.
*/
#ifndef  GSTV4L2ENC_H
#define  GSTV4L2ENC_H

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideoencoder.h>
#include <gst/video/gstvideopool.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <linux/videodev2.h>

#ifdef HAVE_LIBV4L2
#include <libv4l2.h>
#else
#define v4l2_fd_open(fd, flags) (fd)
#define v4l2_close    close
#define v4l2_dup      dup
#define v4l2_ioctl    ioctl
#define v4l2_read     read
#define v4l2_mmap     mmap
#define v4l2_munmap   munmap
#endif

/* Begin Declaration */
G_BEGIN_DECLS
#define GST_TYPE_V4L2ENC	(gst_v4l2enc_get_type())
#define GST_V4L2ENC(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_V4L2ENC, GstV4L2Enc))
#define GST_V4L2ENC_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_V4L2ENC, GstV4L2EncClass))
#define GST_IS_V4L2ENC(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_V4L2ENC))
#define GST_IS_V4L2ENC_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_V4L2ENC))
typedef struct _GstV4L2Enc GstV4L2Enc;
typedef struct _GstV4L2EncClass GstV4L2EncClass;

#include <gstv4l2encbufferpool.h>

struct _GstV4L2Enc
{
  GstVideoEncoder parent;

  gchar *device_name;

  gint fd;

  void **mmap_virtual_output;   /* Pointer tab of output frames */
  gint *mmap_size_output;

  GstBufferPool *pool;          /* Pool of input frames */

  gint current_nb_buf_output;   /* used for output munmaping */

  /* Structure representing the state of an incoming or outgoing video stream
     for encoders and decoders. */
  GstVideoCodecState *input_state;
  GstVideoCodecState *output_state;

  gint width;
  gint height;

  /* properties */
  guint cpb_size;               /* in kbps */
  guint bitrate;                /* in kbps */
  guint bitrate_mode;
  guint level;
  guint pixel_aspect_ratio;
  guint profile;
  guint gop_size;
  guint intra_refresh;
  gboolean dct8x8;
  gboolean cabac;
  guint qpmin;
  guint qpmax;

  GstCaps *caps;
  GstVideoInfo info;
};

struct _GstV4L2EncClass
{

  GstVideoEncoderClass parent_class;
};

GType gst_v4l2enc_get_type (void);

gchar *v4l2_fmt_str (guint32 fmt);

G_END_DECLS
#endif /* GSTV4L2ENC_H */
