/*
* Copyright (C) 2013 STMicroelectronics SA
*
* Author: <hugues.fruchet@st.com> for STMicroelectronics.
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <linux/videodev2.h>
#include "gstv4l2dec.h"
#include <gst/gst.h>


GST_DEBUG_CATEGORY (gst_v4l2dec_debug);
#define GST_CAT_DEFAULT gst_v4l2dec_debug

#define MAX_DEVICES 20          /* Max V4L2 device instances tried */

#define NB_BUF_INPUT 1
#define NB_BUF_OUTPUT 2         /* nb frames necessary for display pipeline */

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#define parent_class gst_v4l2dec_parent_class
G_DEFINE_TYPE (GstV4L2Dec, gst_v4l2dec, GST_TYPE_VIDEO_DECODER);


enum
{
  PROP_0,
  PROP_DEVICE,
};

/* GstVideoDecoder base class method */
static gboolean gst_v4l2dec_start (GstVideoDecoder * decoder);
static gboolean gst_v4l2dec_stop (GstVideoDecoder * video_decoder);
static void gst_v4l2dec_release_pending_buffers (GstVideoDecoder * decoder);
static gboolean gst_v4l2dec_set_format (GstVideoDecoder * decoder,
    GstVideoCodecState * state);
static GstFlowReturn gst_v4l2dec_handle_frame (GstVideoDecoder * decoder,
    GstVideoCodecFrame * frame);
static void gst_v4l2dec_finalize (GObject * object);
static gboolean gst_v4l2dec_decide_allocation (GstVideoDecoder * decoder,
    GstQuery * query);

static void unmap_input_buf (GstV4L2Dec * dec);
static __u32 to_v4l2_streamformat (GstStructure * s);
static gchar *v4l2_type_str (guint32 fmt);
static gchar *v4l2_fmt_str (guint32 pxfmt);

static GstFlowReturn
gst_v4l2dec_decode (GstVideoDecoder * decoder, GstVideoCodecFrame * frame);
static void frame_push_thread (void *arg);
static void frame_recycle_thread (void *arg);
gboolean plugin_init (GstPlugin * plugin);

struct pixel_format
{
  guint32 pixel_fmt_nb;
  gchar *pixel_fmt_str;
};

struct type_io_v4l2
{
  guint32 type_io_nb;
  gchar *type_io_str;
};

struct type_io_v4l2 type_io[] = {
  {V4L2_BUF_TYPE_VIDEO_OUTPUT, (gchar *) "V4L2_BUF_TYPE_VIDEO_OUTPUT"},
  {V4L2_BUF_TYPE_VIDEO_CAPTURE, (gchar *) "V4L2_BUF_TYPE_VIDEO_CAPTURE"},
};

static GstStaticPadTemplate gst_v4l2dec_sink_template =
    GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-h264,"
        "stream-format = (string) { byte-stream },"
        "alignment = (string) { au }"
        ";"
        "video/x-h265,"
        "stream-format = (string) { byte-stream },"
        "alignment = (string) { au }"
        ";"
        "video/mpeg,"
        "mpegversion = (int) { 1, 2, 4 },"
        "systemstream = (boolean) false,"
        "parsed = (boolean) true"
        ";"
        "video/x-xvid;"
        "video/x-3ivx;"
        "video/x-divx,"
        "divxversion = (int) {3, 4, 5},"
        "parsed = (boolean) true"
        ";"
        "video/x-vp8"
        ";"
        "video/x-wmv,"
        "wmvversion = (int) 3"
        ";"
        "video/x-jpeg,"
        "parsed = (boolean) true"
        ";" "image/jpeg," "parsed = (boolean) true" ";")
    );

static GstStaticPadTemplate gst_v4l2dec_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw, "
        "format = (string) NV12, "
        "width  = (int) [ 32, 4096 ], " "height =  (int) [ 32, 4096 ]"));

struct pixel_format px_formats[] = {
  {V4L2_PIX_FMT_NV12, (gchar *) "V4L2_PIX_FMT_NV12"},
  {V4L2_PIX_FMT_H264, (gchar *) "V4L2_PIX_FMT_H264"},
#ifdef V4L2_PIX_FMT_HEVC
  {V4L2_PIX_FMT_HEVC, (gchar *) "V4L2_PIX_FMT_HEVC"},
#endif
  {V4L2_PIX_FMT_MPEG1, (gchar *) "V4L2_PIX_FMT_MPEG1"},
  {V4L2_PIX_FMT_MPEG2, (gchar *) "V4L2_PIX_FMT_MPEG2"},
  {V4L2_PIX_FMT_MPEG4, (gchar *) "V4L2_PIX_FMT_MPEG4"},
  {V4L2_PIX_FMT_XVID, (gchar *) "V4L2_PIX_FMT_XVID"},
  {V4L2_PIX_FMT_VP8, (gchar *) "V4L2_PIX_FMT_VP8"},
  {V4L2_PIX_FMT_VC1_ANNEX_G, (gchar *) "V4L2_PIX_FMT_VC1_ANNEX_G"},
  {V4L2_PIX_FMT_VC1_ANNEX_L, (gchar *) "V4L2_PIX_FMT_VC1_ANNEX_L"},
  {V4L2_PIX_FMT_MJPEG, (gchar *) "V4L2_PIX_FMT_MJPEG"},
};

static const gchar *interlace_mode[] = {
  "progressive",
  "interleaved",
  "mixed",
  "fields"
};

/*
 * meta data for buffers acquired from downstream pool
 */
GType
gst_v4l2dec_downstream_meta_api_get_type (void)
{
  static volatile GType type;
  static const gchar *tags[] = { "downstream", NULL };

  if (g_once_init_enter (&type)) {
    GType _type =
        gst_meta_api_type_register ("GstV4L2DecDownstreamMetaAPI", tags);
    g_once_init_leave (&type, _type);
  }
  return type;
}

const GstMetaInfo *
gst_v4l2dec_downstream_meta_get_info (void)
{
  static const GstMetaInfo *meta_info = NULL;

  if (g_once_init_enter (&meta_info)) {
    const GstMetaInfo *meta =
        gst_meta_register (gst_v4l2dec_downstream_meta_api_get_type (),
        "GstV4L2DecDownstreamMeta",
        sizeof (GstV4L2DecDownstreamMeta), (GstMetaInitFunction) NULL,
        (GstMetaFreeFunction) NULL, (GstMetaTransformFunction) NULL);
    g_once_init_leave (&meta_info, meta);
  }
  return meta_info;
}

static GstVideoInterlaceMode
gst_interlace_mode_from_string (const gchar * mode)
{
  gint i;
  for (i = 0; i < G_N_ELEMENTS (interlace_mode); i++) {
    if (g_str_equal (interlace_mode[i], mode))
      return i;
  }
  return GST_VIDEO_INTERLACE_MODE_PROGRESSIVE;
}

static __u32
to_v4l2_streamformat (GstStructure * s)
{
  if (gst_structure_has_name (s, "video/x-h264"))
    return V4L2_PIX_FMT_H264;

#ifdef V4L2_PIX_FMT_HEVC
  if (gst_structure_has_name (s, "video/x-h265"))
    return V4L2_PIX_FMT_HEVC;
#endif

  if (gst_structure_has_name (s, "video/mpeg")) {
    gint mpegversion = 0;
    if (gst_structure_get_int (s, "mpegversion", &mpegversion)) {
      switch (mpegversion) {
        case 1:
          return V4L2_PIX_FMT_MPEG1;
          break;
        case 2:
          return V4L2_PIX_FMT_MPEG2;
          break;
        case 4:
          return V4L2_PIX_FMT_MPEG4;
          break;
        default:
          return 0;
          break;
      }
    }
  }

  if (gst_structure_has_name (s, "video/x-xvid") ||
      gst_structure_has_name (s, "video/x-3ivx") ||
      gst_structure_has_name (s, "video/x-divx"))
    return V4L2_PIX_FMT_XVID;

  if (gst_structure_has_name (s, "video/x-vp8"))
    return V4L2_PIX_FMT_VP8;

  if (gst_structure_has_name (s, "video/x-wmv")) {
    const gchar *format;

    if ((format = gst_structure_get_string (s, "format"))
        && (g_str_equal (format, "WVC1")))
      return V4L2_PIX_FMT_VC1_ANNEX_G;
    else
      return V4L2_PIX_FMT_VC1_ANNEX_L;
  }

  if (gst_structure_has_name (s, "image/jpeg"))
    return V4L2_PIX_FMT_MJPEG;

  return 0;
}

/* used for debug I/O type (v4l2 object) */
static gchar *
v4l2_type_str (guint32 type)
{
  int i = 0;
  for (i = 0; i < ARRAY_SIZE (type_io); i++) {
    if (type_io[i].type_io_nb == type)
      return type_io[i].type_io_str;
  }
  return NULL;
}

/* used for debug pixelformat (v4l2 object) */
static gchar *
v4l2_fmt_str (guint32 fmt)
{
  int i = 0;
  for (i = 0; i < ARRAY_SIZE (px_formats); i++) {
    if (px_formats[i].pixel_fmt_nb == fmt)
      return px_formats[i].pixel_fmt_str;
  }
  return NULL;
}


static GstVideoFormat
to_gst_pixelformat (__u32 fmt)
{
  switch (fmt) {
    case V4L2_PIX_FMT_NV12:
      return GST_VIDEO_FORMAT_NV12;
    default:
      return GST_VIDEO_FORMAT_UNKNOWN;
  }
}

/* Open the device matching width/height/format as input */
static int
gst_v4l2dec_open_device (GstV4L2Dec * dec, __u32 fmt, __u32 width, __u32 height,
    GstVideoInterlaceMode interlace_mode)
{
  int fd = -1;
  int ret;
  gint i = 0;
  gboolean found;
  gchar path[100];
  struct v4l2_format try_fmt;
  struct v4l2_format s_fmt;
  int libv4l2_fd;

  memset (&try_fmt, 0, sizeof try_fmt);
  try_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  try_fmt.fmt.pix.pixelformat = fmt;

  memset (&s_fmt, 0, sizeof s_fmt);
  s_fmt.fmt.pix.width = width;
  s_fmt.fmt.pix.height = height;
  s_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

  /* particular case of MJPEG : pixel format could be different that YUV420 */
  if (fmt == V4L2_PIX_FMT_MJPEG)
    s_fmt.fmt.pix.sizeimage = (width * height); /* for MJPEG, let's take YUV422 (w*h*2) largely with /2 compression (so w*h *2 / 2) */
  else
    s_fmt.fmt.pix.sizeimage = ((width * height * 3 / 2) / 2);   /* expecting video compression to acheive /2 */

  s_fmt.fmt.pix.pixelformat = fmt;

  if (interlace_mode == GST_VIDEO_INTERLACE_MODE_PROGRESSIVE)
    s_fmt.fmt.pix.field = V4L2_FIELD_NONE;
  else
    s_fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

  found = FALSE;
  for (i = 0; i < MAX_DEVICES; i++) {
    snprintf (path, sizeof (path), "/dev/video%d", i);

    fd = open (path, O_RDWR, 0);
    if (fd < 0)
      continue;

    libv4l2_fd = v4l2_fd_open (fd, V4L2_DISABLE_CONVERSION);
    if (libv4l2_fd != -1)
      fd = libv4l2_fd;

    ret = v4l2_ioctl (fd, VIDIOC_TRY_FMT, &try_fmt);
    if (ret < 0) {
      v4l2_close (fd);
      continue;
    }

    ret = v4l2_ioctl (fd, VIDIOC_S_FMT, &s_fmt);
    if (ret < 0) {
      v4l2_close (fd);
      continue;
    }

    found = TRUE;
    break;
  }

  if (!found) {
    GST_ERROR_OBJECT (dec,
        "No device found matching format %s(0x%x) and resolution %dx%d",
        v4l2_fmt_str (fmt), fmt, width, height);
    return -1;
  }

  GST_INFO_OBJECT (dec, "Device %s opened for format %s and %dx%d resolution",
      path, v4l2_fmt_str (fmt), width, height);
  return fd;
}

static void
gst_v4l2dec_process_codec_data (GstV4L2Dec * dec,
    __u32 streamformat, GstBuffer * codec_data)
{
  if (streamformat == V4L2_PIX_FMT_H264) {
    GstBuffer *header = NULL;
    unsigned int sps_size;
    unsigned int pps_size;
    GstMapInfo header_mapinfo = { 0, };
    GstMapInfo mapinfo = { 0, };
    unsigned char *codec_data_ptr;
    unsigned char *sps_pps_ptr;

    header = gst_buffer_new_and_alloc (gst_buffer_get_size (codec_data));
    gst_buffer_map (header, &header_mapinfo, GST_MAP_WRITE);
    sps_pps_ptr = header_mapinfo.data;

    gst_buffer_map (codec_data, &mapinfo, GST_MAP_READ);
    codec_data_ptr = mapinfo.data;

    /* Header decomposition */
    /* <7 bytes><SPS header size><SPS Header><3 bytes><PPS Header> */

    /* <7 bytes> to skip */
    codec_data_ptr += 7;

    /* <SPS header size> on 1 byte */
    sps_size = *codec_data_ptr;
    codec_data_ptr += 1;

    /* 4 bytes SPS startcode to add */
    sps_pps_ptr[0] = 0x00;
    sps_pps_ptr[1] = 0x00;
    sps_pps_ptr[2] = 0x00;
    sps_pps_ptr[3] = 0x01;
    sps_pps_ptr += 4;

    /* <SPS> on <SPS header size> bytes */
    memcpy (sps_pps_ptr, codec_data_ptr, sps_size);
    codec_data_ptr += sps_size;
    sps_pps_ptr += sps_size;

    /* <2 bytes> to skip */
    codec_data_ptr += 2;

    /* <PPS header size> on 1 byte */
    pps_size = *codec_data_ptr;
    codec_data_ptr += 1;

    /* 4 bytes PPS startcode to add */
    sps_pps_ptr[0] = 0x00;
    sps_pps_ptr[1] = 0x00;
    sps_pps_ptr[2] = 0x00;
    sps_pps_ptr[3] = 0x01;
    sps_pps_ptr += 4;

    /* <PPS> on <PPS header size> bytes */
    memcpy (sps_pps_ptr, codec_data_ptr, pps_size);

    sps_size += 4;
    pps_size += 4;

    gst_buffer_unmap (codec_data, &mapinfo);
    gst_buffer_unmap (header, &header_mapinfo);

    gst_buffer_set_size (header, sps_size + pps_size);
    dec->header = header;
  }
}

gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "v4l2dec", GST_RANK_PRIMARY + 1,
          GST_TYPE_V4L2DEC))
    return FALSE;
  return TRUE;
}

static void
gst_v4l2dec_class_init (GstV4L2DecClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstVideoDecoderClass *video_decoder_class = GST_VIDEO_DECODER_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->finalize = gst_v4l2dec_finalize;

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_v4l2dec_src_template));

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_v4l2dec_sink_template));

  video_decoder_class->start = GST_DEBUG_FUNCPTR (gst_v4l2dec_start);
  video_decoder_class->stop = GST_DEBUG_FUNCPTR (gst_v4l2dec_stop);
  video_decoder_class->set_format = GST_DEBUG_FUNCPTR (gst_v4l2dec_set_format);
  video_decoder_class->handle_frame =
      GST_DEBUG_FUNCPTR (gst_v4l2dec_handle_frame);
  video_decoder_class->decide_allocation =
      GST_DEBUG_FUNCPTR (gst_v4l2dec_decide_allocation);

  GST_DEBUG_CATEGORY_INIT (gst_v4l2dec_debug, "v4l2dec", 0,
      "ST v4l2 video decoder");

  gst_element_class_set_static_metadata (element_class,
      "V4L2 decoder", "Decoder/Video", "A v4l2 decoder", "STMicroelectronics");
}

static gboolean
gst_v4l2dec_decide_allocation (GstVideoDecoder * decoder, GstQuery * query)
{
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GstCaps *outcaps;
  guint allocator_nb, i;
  GstAllocator *allocator;
  GstAllocationParams params;
  GstStructure *config;
  struct v4l2_requestbuffers reqbufs;

  gst_query_parse_allocation (query, &outcaps, NULL);

  /* get the allocator array size */
  allocator_nb = gst_query_get_n_allocation_params (query);
  GST_DEBUG_OBJECT (decoder, "Got %d allocator(s)", allocator_nb);

  if (allocator_nb > 0) {
    /* parse the allocators: select the dmabuf allocator if present,
     * else select the first memory allocator in the query
     */
    for (i = 0; i < allocator_nb; i++) {
      gst_query_parse_nth_allocation_param (query, i, &allocator, &params);
      if (allocator) {
        GstBufferPool *pool;
        guint size, min_buffers, max_buffers;

        GST_DEBUG_OBJECT (decoder, "Got %s allocator", allocator->mem_type);

        if (!g_strcmp0 (allocator->mem_type, GST_ALLOCATOR_DMABUF)) {
          /* If already configured/active, simply read back the active configuration */
          /* FIXME not entirely correct, See bug 728268 */
          if (dec->downstream_pool
              && gst_buffer_pool_is_active (dec->downstream_pool)) {
            GST_DEBUG_OBJECT (decoder, "Pool %s is already active",
                dec->downstream_pool->object.name);
            config = gst_buffer_pool_get_config (dec->downstream_pool);
            gst_buffer_pool_config_get_params (config, NULL, &size,
                &min_buffers, &max_buffers);
            gst_structure_free (config);

            gst_query_add_allocation_pool (query, dec->downstream_pool, size,
                min_buffers, max_buffers);
            gst_object_unref (dec->downstream_pool);

            return TRUE;
          } else {
            gst_query_set_nth_allocation_param (query, 0, allocator, &params);

            gst_query_parse_nth_allocation_pool (query, 0, &pool, &size,
                &min_buffers, &max_buffers);

            GST_DEBUG_OBJECT (decoder,
                "Found %s downstream DMA-Buf pool %p with min=%d max=%d ",
                pool->object.name, pool, min_buffers, max_buffers);

            /* Request output buffers needed (DMABUF mode) */
            memset (&reqbufs, 0, sizeof reqbufs);
            reqbufs.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            reqbufs.count = (min_buffers == 0 ? NB_BUF_OUTPUT : min_buffers);
            reqbufs.memory = V4L2_MEMORY_DMABUF;

            if (v4l2_ioctl (dec->fd, VIDIOC_REQBUFS, &reqbufs) < 0)
              goto error_ioc_reqbufs;

            /* request that downstream pool allocates at least driver need (reqbufs.count) */
            min_buffers = max_buffers = reqbufs.count;

            /* specify the buffer size */
            size = dec->size_image;

            gst_query_add_allocation_pool (query, pool, size, min_buffers,
                max_buffers);
            /* we put our prefered pool at first position because
             * video decoder base class is always selecting the first */
            gst_query_set_nth_allocation_pool (query, 0, pool, size,
                min_buffers, max_buffers);
            GST_DEBUG_OBJECT (decoder,
                "Set downstream DMA-Buf pool %p to min=%d max=%d ", pool,
                min_buffers, max_buffers);
            config = gst_buffer_pool_get_config (pool);
            if (gst_query_find_allocation_meta (query, GST_VIDEO_META_API_TYPE,
                    NULL)) {
              gst_buffer_pool_config_add_option (config,
                  GST_BUFFER_POOL_OPTION_VIDEO_META);
              gst_buffer_pool_config_add_option (config,
                  GST_BUFFER_POOL_OPTION_VIDEO_ALIGNMENT);
              GST_LOG_OBJECT (decoder,
                  "Set downstream pool %p padding to %u-%ux%u-%u", pool,
                  dec->align.padding_top, dec->align.padding_left,
                  dec->align.padding_right, dec->align.padding_bottom);
              gst_buffer_pool_config_set_video_alignment (config, &dec->align);
              gst_buffer_pool_set_config (pool, config);

              /* Select this downstream pool */
              dec->downstream_pool = pool;
              /* ref it because we are using this pool without informing base class */
              gst_object_ref (dec->downstream_pool);
            }
          }
          gst_object_unref (allocator);
        }
      } else {
        GST_DEBUG_OBJECT (decoder, "Allocator is NULL (number of allocator=%d)",
            allocator_nb);
      }
    }
  }

  return GST_VIDEO_DECODER_CLASS (parent_class)->decide_allocation (decoder,
      query);

  /* ERRORS */
error_ioc_reqbufs:
  {
    GST_ERROR_OBJECT (dec, "Unable to request output buffers err=%s",
        strerror (errno));
    return FALSE;
  }
}

/* Init the v4l2dec structure */
static void
gst_v4l2dec_init (GstV4L2Dec * dec)
{
  GstVideoDecoder *decoder = (GstVideoDecoder *) dec;

  dec->current_nb_buf_input = 0;

  gst_video_decoder_set_packetized (decoder, TRUE);

  dec->mmap_virtual_input = NULL;
  dec->mmap_size_input = NULL;

  dec->output_setup = FALSE;
  dec->input_setup = FALSE;

  dec->frame_push_task = gst_task_new (frame_push_thread, decoder, NULL);
  g_rec_mutex_init (&dec->frame_push_task_mutex);
  gst_task_set_lock (dec->frame_push_task, &dec->frame_push_task_mutex);

  dec->frame_recycle_task = gst_task_new (frame_recycle_thread, decoder, NULL);
  g_rec_mutex_init (&dec->frame_recycle_task_mutex);
  gst_task_set_lock (dec->frame_recycle_task, &dec->frame_recycle_task_mutex);

  dec->header = NULL;
  dec->fd = -1;
}

static void
gst_v4l2dec_finalize (GObject * object)
{
  GstV4L2Dec *dec = GST_V4L2DEC (object);

  if (gst_task_get_state (dec->frame_push_task) != GST_TASK_STOPPED) {
    GST_ERROR_OBJECT (dec, "task %p should be stopped by now",
        dec->frame_push_task);
    gst_task_stop (dec->frame_push_task);
    g_rec_mutex_lock (&dec->frame_push_task_mutex);
    g_rec_mutex_unlock (&dec->frame_push_task_mutex);
    gst_task_join (dec->frame_push_task);
  }

  gst_object_unref (dec->frame_push_task);
  dec->frame_push_task = NULL;
  g_rec_mutex_clear (&dec->frame_push_task_mutex);

  if (gst_task_get_state (dec->frame_recycle_task) != GST_TASK_STOPPED) {
    GST_ERROR_OBJECT (dec, "task %p should be stopped by now",
        dec->frame_recycle_task);
    gst_task_stop (dec->frame_recycle_task);
    g_rec_mutex_lock (&dec->frame_recycle_task_mutex);
    g_rec_mutex_unlock (&dec->frame_recycle_task_mutex);
    gst_task_join (dec->frame_recycle_task);
  }

  gst_object_unref (dec->frame_recycle_task);
  dec->frame_recycle_task = NULL;
  g_rec_mutex_clear (&dec->frame_recycle_task_mutex);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

/* Open the device */
static gboolean
gst_v4l2dec_start (GstVideoDecoder * decoder)
{
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GST_DEBUG_OBJECT (dec, "Starting");

  return TRUE;
}

/* Stop Stream + Munmaping + Close thread and device */
static gboolean
gst_v4l2dec_stop (GstVideoDecoder * decoder)
{
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  gint type;
  GST_DEBUG_OBJECT (dec, "Stopping");

  dec->input_setup = FALSE;
  dec->output_setup = FALSE;

  if (dec->header) {
    gst_buffer_unref (dec->header);
    dec->header = NULL;
  }

  type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  if (v4l2_ioctl (dec->fd, VIDIOC_STREAMOFF, &type) < 0)
    GST_WARNING_OBJECT (dec, "Unable to stop stream on output err=%s",
        strerror (errno));

  GST_DEBUG_OBJECT (dec, "STREAM OFF OUTPUT");

  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (v4l2_ioctl (dec->fd, VIDIOC_STREAMOFF, &type) < 0)
    GST_WARNING_OBJECT (dec, "Unable to stop stream on capture err=%s",
        strerror (errno));

  GST_DEBUG_OBJECT (dec, "STREAM OFF CAPTURE");

  if (dec->input_state) {
    gst_video_codec_state_unref (dec->input_state);
    dec->input_state = NULL;
  }
  if (dec->output_state) {
    gst_video_codec_state_unref (dec->output_state);
    dec->output_state = NULL;
  }

  if (dec->downstream_pool) {
    gst_v4l2dec_release_pending_buffers (decoder);
    gst_buffer_pool_set_active (dec->downstream_pool, FALSE);
  }

  if (dec->pool) {
    gst_buffer_pool_set_active (dec->pool, FALSE);
  }

  if (dec->frame_push_task) {
    gst_task_stop (dec->frame_push_task);
    g_rec_mutex_lock (&dec->frame_push_task_mutex);
    g_rec_mutex_unlock (&dec->frame_push_task_mutex);
    gst_task_join (dec->frame_push_task);
  }

  if (dec->frame_recycle_task) {
    gst_task_stop (dec->frame_recycle_task);
    g_rec_mutex_lock (&dec->frame_recycle_task_mutex);
    g_rec_mutex_unlock (&dec->frame_recycle_task_mutex);
    gst_task_join (dec->frame_recycle_task);
  }

  /* release downstream pool */
  if (dec->downstream_pool) {
    gst_object_unref (dec->downstream_pool);
    dec->downstream_pool = NULL;
  }

  if (dec->pool) {
    gst_object_unref (dec->pool);
    dec->pool = NULL;
  }

  unmap_input_buf (dec);

  if (dec->mmap_virtual_input) {
    free (dec->mmap_virtual_input);
    dec->mmap_virtual_input = NULL;

    free (dec->mmap_size_input);
    dec->mmap_size_input = NULL;

    dec->current_nb_buf_input = 0;
  }

  if (dec->fd != -1) {
    v4l2_close (dec->fd);
    dec->fd = -1;
  }

  GST_DEBUG_OBJECT (dec, "Stopped !!");

  return TRUE;
}

/* release buffers acquired from downstream pool */
/* should be called after capture stream off */
static void
gst_v4l2dec_release_pending_buffers (GstVideoDecoder * decoder)
{
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GstV4L2DecDownstreamMeta *meta = NULL;
  gint i;

  for (i = 0; i < ARRAY_SIZE (dec->downstream_buffers); i++) {
    GstBuffer *buffer = dec->downstream_buffers[i];
    if (buffer) {
      meta = GST_V4L2DEC_DOWNSTREAM_META_GET (buffer);
      if (meta && meta->acquired) {
        GST_DEBUG_OBJECT (dec,
            "pending acquired buffer %p, index %d, force release", buffer, i);
        gst_buffer_unref (buffer);
      }
    }
  }
  return;
}

/* Setup input (Output for V4L2) */
static gboolean
gst_v4l2dec_set_format (GstVideoDecoder * decoder, GstVideoCodecState * state)
{
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GstStructure *structure;
  const gchar *s;
  gint i;
  gint type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  gint retval = 0;
  gint width = 0;
  gint height = 0;
  GstVideoInterlaceMode interlace_mode;

  __u32 streamformat;
  int fd;
  struct v4l2_requestbuffers reqbuf;
  struct v4l2_buffer querybuf;
  struct v4l2_buffer qbuf;

  GST_DEBUG_OBJECT (dec, "Setting format: %" GST_PTR_FORMAT, state->caps);

  structure = gst_caps_get_structure (state->caps, 0);

  streamformat = to_v4l2_streamformat (structure);
  if (!streamformat)
    goto error_format;

  retval = gst_structure_get_int (structure, "width", &width);
  retval &= gst_structure_get_int (structure, "height", &height);
  if (!retval)
    goto error_res;

  if ((s = gst_structure_get_string (structure, "interlace-mode")))
    interlace_mode = gst_interlace_mode_from_string (s);
  else
    interlace_mode = GST_VIDEO_INTERLACE_MODE_PROGRESSIVE;

  if (dec->input_setup) {
    /* Already setup, check to see if something has changed on input caps... */
    if ((dec->streamformat == streamformat) &&
        (dec->width == width) && (dec->height == height)) {
      goto done;                /* Nothing has changed */
    } else {
      /* V4L2_DEC_CMD_STOP: this command should be called on v4l2 decoder
       * to force decoder to decode all pending buffers and put an empty buffer
       * at end with V4L2_BUF_FLAG_LAST flag in V4L2 capture outgoing queue.
       * TBD in Ticket 74968 - require VIDIOC_DECODER_CMD in v4l2 driver
       */
      GST_FIXME_OBJECT (dec, "TBD: require V4L2_DEC_CMD_STOP in v4l2 driver");
      gst_v4l2dec_stop (decoder);
    }
  }

  fd = gst_v4l2dec_open_device (dec, streamformat, width, height,
      interlace_mode);
  if (fd == -1)
    goto error_device;

  dec->fd = fd;
  dec->streamformat = streamformat;
  dec->width = width;
  dec->height = height;

  if (dec->input_state)
    gst_video_codec_state_unref (dec->input_state);
  dec->input_state = gst_video_codec_state_ref (state);

  /* Header */
  dec->codec_data = state->codec_data;
  if (dec->codec_data)
    gst_v4l2dec_process_codec_data (dec, streamformat, dec->codec_data);

  /* Memory mapping for input buffers in V4L2 */
  memset (&reqbuf, 0, sizeof reqbuf);
  reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  reqbuf.count = NB_BUF_INPUT;
  reqbuf.memory = V4L2_MEMORY_MMAP;
  if (v4l2_ioctl (dec->fd, VIDIOC_REQBUFS, &reqbuf) < 0)
    goto error_ioc_reqbufs;

  dec->mmap_virtual_input = malloc (sizeof (void *) * reqbuf.count);
  dec->mmap_size_input = malloc (sizeof (void *) * reqbuf.count);

  memset (&querybuf, 0, sizeof querybuf);
  querybuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  querybuf.memory = V4L2_MEMORY_MMAP;
  for (i = 0; i < reqbuf.count; i++) {
    void *ptr;
    querybuf.index = i;
    dec->current_nb_buf_input++;

    /* Memory mapping for input buffers in GStreamer */
    if (v4l2_ioctl (dec->fd, VIDIOC_QUERYBUF, &querybuf) < 0)
      goto error_ioc_querybuf;
    ptr = v4l2_mmap (NULL, querybuf.length, PROT_READ | PROT_WRITE,
        MAP_SHARED, dec->fd, querybuf.m.offset);
    if (ptr == MAP_FAILED)
      goto error_map_fail;

    dec->mmap_virtual_input[i] = ptr;
    dec->mmap_size_input[i] = querybuf.length;

    qbuf = querybuf;            /* index from querybuf */
    qbuf.bytesused = 0;         /* enqueue it with no data */
    if (v4l2_ioctl (dec->fd, VIDIOC_QBUF, &qbuf) < 0)
      goto error_ioc_qbuf;
  }

  /* Start stream on input */
  if (v4l2_ioctl (dec->fd, VIDIOC_STREAMON, &type) < 0)
    goto error_ioc_streamon;

  dec->input_setup = TRUE;

done:
  return TRUE;

  /* Errors */
error_format:
  {
    GST_ERROR_OBJECT (dec, "Unsupported format in caps: %" GST_PTR_FORMAT,
        state->caps);
    return FALSE;
  }
error_res:
  {
    GST_ERROR_OBJECT (dec, "Unable to get width/height value");
    return FALSE;
  }
error_device:
  {
    return FALSE;
  }
error_ioc_reqbufs:
  {
    GST_ERROR_OBJECT (dec, "Unable to request input buffers err=%s",
        strerror (errno));
    return FALSE;
  }
error_ioc_querybuf:
  {
    GST_ERROR_OBJECT (dec, "Query of input buffer failed err=%s",
        strerror (errno));
    return FALSE;
  }
error_map_fail:
  {
    GST_ERROR_OBJECT (dec, "Failed to map input buffer");
    return FALSE;
  }
error_ioc_qbuf:
  {
    GST_ERROR_OBJECT (dec, "Enqueuing buffer failed err=%s", strerror (errno));
    unmap_input_buf (dec);
    return FALSE;
  }
error_ioc_streamon:
  {
    GST_ERROR_OBJECT (dec, "Unable to start input stream err=%s",
        strerror (errno));
    unmap_input_buf (dec);
    return FALSE;
  }
}

static void
unmap_input_buf (GstV4L2Dec * dec)
{
  if (dec->mmap_virtual_input)
    for (int i = 0; i < dec->current_nb_buf_input; i++)
      v4l2_munmap (dec->mmap_virtual_input[i], dec->mmap_size_input[i]);
}

/* setup output (Capture for V4L2) with the Header if not yet done
 * else : go to decode for each AU available */
static GstFlowReturn
gst_v4l2dec_setup_output (GstVideoDecoder * decoder, GstVideoCodecFrame * frame)
{
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GstFlowReturn ret = GST_FLOW_OK;

  GstBuffer *header = NULL;
  GstMapInfo mapinfo = { 0, };

  GstVideoAlignment align;

  struct v4l2_buffer dqbuf;
  struct v4l2_format g_fmt;
  struct v4l2_crop g_crop;
  struct v4l2_buffer qbuf;
  struct v4l2_requestbuffers reqbufs;
  gint type;
  void *au_data;
  int au_size;
  void *v4l2_data;
  __u32 v4l2_size;
  GstBufferPool *pool;
  __u32 width, height, aligned_width, aligned_height;
  __u32 fmt;
  __u32 size;

  /* Dequeue a V4L2 buffer where to write */
  memset (&dqbuf, 0, sizeof dqbuf);
  dqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  dqbuf.memory = V4L2_MEMORY_MMAP;
  if (v4l2_ioctl (dec->fd, VIDIOC_DQBUF, &dqbuf) < 0)
    goto error_ioc_dqbuf;

  v4l2_data = dec->mmap_virtual_input[dqbuf.index];
  v4l2_size = dqbuf.length;

  /* Copy header in V4L2 buffer */
  if (dec->header)
    header = dec->header;       /* Header derived from codec_data processing */
  else if (dec->codec_data)
    header = dec->codec_data;   /* Header is codec_data */
  else
    header = frame->input_buffer;       /* Header is within input gst buffer */

  gst_buffer_map (header, &mapinfo, GST_MAP_READ);
  au_data = mapinfo.data;
  au_size = mapinfo.size;
  if (au_size > v4l2_size) {
    gst_buffer_unmap (header, &mapinfo);
    goto error_au_size;
  }
  memcpy (v4l2_data, au_data, au_size);
  gst_buffer_unmap (header, &mapinfo);

  /* Queue V4L2 buffer */
  qbuf = dqbuf;                 /* index from dqbuf */
  qbuf.bytesused = au_size;
  if (v4l2_ioctl (dec->fd, VIDIOC_QBUF, &qbuf) < 0)
    goto error_ioc_qbuf;

  /* Get output frame format from V4L2 (read from header) */
  memset (&g_fmt, 0, sizeof g_fmt);
  g_fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  ret = v4l2_ioctl (dec->fd, VIDIOC_G_FMT, &g_fmt);
  if (ret != 0)
    goto done;                  /* Header not yet found */

  GST_DEBUG_OBJECT (dec,
      "Format found from V4L2 :fmt:%s, width:%d, height:%d, bytesperline:%d, sizeimage:%d, pixelfmt:%s, field:%d",
      v4l2_type_str (g_fmt.type), g_fmt.fmt.pix.width,
      g_fmt.fmt.pix.height, g_fmt.fmt.pix.bytesperline,
      g_fmt.fmt.pix.sizeimage, v4l2_fmt_str (g_fmt.fmt.pix.pixelformat),
      g_fmt.fmt.pix.field);

  aligned_width = g_fmt.fmt.pix.width;
  aligned_height = g_fmt.fmt.pix.height;
  fmt = g_fmt.fmt.pix.pixelformat;
  size = g_fmt.fmt.pix.sizeimage;

  /* Compute padding */
  gst_video_alignment_reset (&align);
  memset (&g_crop, 0, sizeof g_crop);
  g_crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (v4l2_ioctl (dec->fd, VIDIOC_G_CROP, &g_crop) < 0) {
    GST_DEBUG_OBJECT (dec, "Not able to get crop, default to %dx%d",
        aligned_width, aligned_height);
    width = aligned_width;
    height = aligned_height;
  } else {
    GST_DEBUG_OBJECT (dec,
        "Crop found from V4L2 :fmt:%s, left:%d, top:%d, width:%d, height:%d",
        v4l2_type_str (g_crop.type), g_crop.c.left, g_crop.c.top,
        g_crop.c.width, g_crop.c.height);
    width = g_crop.c.width;
    height = g_crop.c.height;

    align.padding_left = g_crop.c.left;
    align.padding_top = g_crop.c.top;
    align.padding_right = aligned_width - align.padding_left - width;
    align.padding_bottom = aligned_height - align.padding_top - height;

    GST_DEBUG_OBJECT (dec,
        "Padding information deduced from V4L2 G_FMT/G_CROP: padding_left:%d, padding_right:%d, "
        "padding_top:%d, padding_bottom:%d",
        align.padding_left, align.padding_right,
        align.padding_top, align.padding_bottom);
  }


  dec->output_state =
      gst_video_decoder_set_output_state (GST_VIDEO_DECODER (dec),
      to_gst_pixelformat (fmt), width, height, dec->input_state);

  switch (g_fmt.fmt.pix.field) {
    case V4L2_FIELD_NONE:
      dec->output_state->info.interlace_mode =
          GST_VIDEO_INTERLACE_MODE_PROGRESSIVE;
      break;
    case V4L2_FIELD_INTERLACED:
    case V4L2_FIELD_INTERLACED_TB:
    case V4L2_FIELD_INTERLACED_BT:
      dec->output_state->info.interlace_mode =
          GST_VIDEO_INTERLACE_MODE_INTERLEAVED;
      break;
    default:
      /* keep the interlace mode as specified by the CAPS */
      break;
  }

  /* Set buffer alignment */
  gst_video_info_align (&dec->output_state->info, &align);
  dec->align = align;

  /* Set buffer size */
  dec->size_image = size;

  gst_video_decoder_negotiate (decoder);        /* will call gst_v4l2dec_decide_allocation */

  pool = dec->downstream_pool;

  if (!pool) {
    /* Request output buffers needed (MMAP mode) */
    memset (&reqbufs, 0, sizeof reqbufs);
    reqbufs.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    reqbufs.count = NB_BUF_OUTPUT;
    reqbufs.memory = V4L2_MEMORY_MMAP;

    if (v4l2_ioctl (dec->fd, VIDIOC_REQBUFS, &reqbufs) < 0)
      goto error_ioc_reqbufs;

    /* construct a new buffer pool */
    dec->pool = gst_v4l2dec_buffer_pool_new (dec, NULL, reqbufs.count,  /* driver nb of buffers needed */
        g_fmt.fmt.pix.sizeimage, &align);
    if (dec->pool == NULL)
      goto error_new_pool;

    /* activate the pool: the buffers are allocated */
    if (gst_buffer_pool_set_active (dec->pool, TRUE) == FALSE)
      goto error_activate_pool;

  } else {
    GstStructure *config = gst_buffer_pool_get_config (pool);
    guint size, min_buffers, max_buffers;
    struct v4l2_buffer qbuf;
    GstV4L2DecDownstreamMeta *meta = NULL;
    GstCaps *caps;
    gint i;

    gst_buffer_pool_config_get_params (config, &caps, &size, &min_buffers,
        &max_buffers);

    GST_DEBUG_OBJECT (dec, "Use downstream pool %p min %d max %d", pool,
        min_buffers, max_buffers);

    for (i = 0; i < ARRAY_SIZE (dec->downstream_buffers); i++)
      dec->downstream_buffers[i] = NULL;

    /* Queue output buffers in order that decoder driver
     * is ready to decode up to dpb frames */
    for (i = 0; i < min_buffers; i++) {
      GstBuffer *buffer;
      GstMemory *gmem = NULL;
      GstBufferPoolAcquireParams params = {.flags =
            GST_BUFFER_POOL_ACQUIRE_FLAG_DONTWAIT
      };
      gint fd = -1;

      ret = gst_buffer_pool_acquire_buffer (pool, &buffer, &params);
      if (ret == GST_FLOW_EOS) {        /* No more buffers available (max reached) */
        GST_LOG_OBJECT (dec, "%d buffers available over %d buffers requested",
            i, min_buffers);
        /* FIXME, this situation occurs if the sink (e.g. encoder) reserves some buffers
         * for its internal need */
        break;
      }
      if (ret != GST_FLOW_OK)   /* logically caused by STREAMOFF */
        goto out;

      dec->downstream_buffers[i] = buffer;
      meta = GST_V4L2DEC_DOWNSTREAM_META_ADD (buffer);
      if (meta) {
        GST_META_FLAG_SET (meta, GST_META_FLAG_POOLED);
        GST_META_FLAG_SET (meta, GST_META_FLAG_LOCKED);
        meta->acquired = TRUE;
        GST_LOG_OBJECT (dec,
            "set acquire TRUE: dmabuf fd %d, buffer %p, index %d", fd, buffer,
            i);
      }

      gmem = gst_buffer_get_memory (buffer, 0);
      if ((gmem != NULL) && (gst_is_dmabuf_memory (gmem) == TRUE)) {
        fd = gst_dmabuf_memory_get_fd (gmem);
        GST_INFO_OBJECT (dec,
            "Setup output buffer: dmabuf fd %d, buffer %p, index %d", fd,
            buffer, i);
      }
      gst_memory_unref (gmem);

      GST_DEBUG_OBJECT (dec, "Queue buffer (capture type)");
      memset (&qbuf, 0, sizeof qbuf);
      qbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      qbuf.memory = V4L2_MEMORY_DMABUF;
      qbuf.index = i;
      qbuf.m.fd = fd;
      qbuf.bytesused = dec->size_image;
      qbuf.length = dec->size_image;
      if (v4l2_ioctl (dec->fd, VIDIOC_QBUF, &qbuf) < 0)
        goto queue_failed;
    }
  }

  /* Start streaming on output */
  type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  if (v4l2_ioctl (dec->fd, VIDIOC_STREAMON, &type) < 0)
    goto error_ioc_streamon;

  /* Everything is ready, start the frame push thread */
  gst_task_start (dec->frame_push_task);

  /* Start the frame recycle thread */
  if (dec->downstream_pool)
    gst_task_start (dec->frame_recycle_task);

  dec->output_setup = TRUE;

done:
  return GST_FLOW_OK;

  /* Errors */
error_ioc_dqbuf:
  {
    GST_ERROR_OBJECT (dec, "Dequeuing failed err=%s", strerror (errno));
    return GST_FLOW_ERROR;
  }
error_au_size:
  {
    GST_ERROR_OBJECT (dec, "Input size too large (%d > %d)", au_size,
        v4l2_size);
    return GST_FLOW_ERROR;
  }
error_ioc_qbuf:
  {
    GST_ERROR_OBJECT (dec, "Enqueuing failed err=%s", strerror (errno));
    return GST_FLOW_ERROR;
  }
error_ioc_reqbufs:
  {
    GST_ERROR_OBJECT (dec, "Unable to request buffers err=%s",
        strerror (errno));
    return GST_FLOW_ERROR;
  }
error_new_pool:
  {
    GST_ERROR_OBJECT (dec, "Unable to construct a new buffer pool");
    return GST_FLOW_ERROR;
  }
error_activate_pool:
  {
    GST_ERROR_OBJECT (dec, "Unable to activate the pool");
    gst_object_unref (dec->pool);
    return GST_FLOW_ERROR;
  }
error_ioc_streamon:
  {
    GST_ERROR_OBJECT (dec, "Streamon failed err=%s", strerror (errno));
    gst_buffer_pool_set_active (GST_BUFFER_POOL_CAST (dec->pool), FALSE);
    gst_object_unref (dec->pool);
    return GST_FLOW_ERROR;
  }
queue_failed:
  {
    GST_ERROR_OBJECT (dec, "Queuing input failed err=%s", strerror (errno));
    return GST_FLOW_ERROR;
  }

out:
  return GST_FLOW_OK;
}


/* setup output if not yet done
 * else : go to decode for each AU available */
static GstFlowReturn
gst_v4l2dec_handle_frame (GstVideoDecoder * decoder, GstVideoCodecFrame * frame)
{
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GstFlowReturn ret = GST_FLOW_OK;

  if (!dec->input_setup)
    return GST_FLOW_OK;

  /* Setup output if not yet done */
  if (!dec->output_setup) {
    ret = gst_v4l2dec_setup_output (decoder, frame);
    if (ret)
      return ret;
  }

  /* For every frame, decode */
  ret = gst_v4l2dec_decode (decoder, frame);

  return ret;
}

/* Here we push AU to V4L2, header found so */
static GstFlowReturn
gst_v4l2dec_decode (GstVideoDecoder * decoder, GstVideoCodecFrame * frame)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstMapInfo mapinfo = { 0, };
  GstBuffer *buf = NULL;
  guint8 *gstdata;
  gsize gstsize;
  struct v4l2_buffer dqbuf;
  struct v4l2_buffer qbuf;

  GstV4L2Dec *dec = GST_V4L2DEC (decoder);

  /* pop an empty buffer */
  memset (&dqbuf, 0, sizeof dqbuf);
  dqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  dqbuf.memory = V4L2_MEMORY_MMAP;
  if (v4l2_ioctl (dec->fd, VIDIOC_DQBUF, &dqbuf) < 0)
    goto error_ioctl_dequeue_out;

  /* copy GST -> V4L2 */
  buf = frame->input_buffer;

  gst_buffer_map (buf, &mapinfo, GST_MAP_READ);
  gstdata = mapinfo.data;
  gstsize = mapinfo.size;

  if (gstsize > dec->mmap_size_input[dqbuf.index]) {
    GST_ERROR_OBJECT (dec,
        "Size exceed, dest (%d) smaller than source (%" G_GSIZE_FORMAT ")\n",
        dec->mmap_size_input[dqbuf.index], gstsize);
    gst_buffer_unmap (buf, &mapinfo);
    return GST_FLOW_ERROR;
  } else {
    memcpy (dec->mmap_virtual_input[dqbuf.index], gstdata, gstsize);
  }

  gst_buffer_unmap (buf, &mapinfo);

  /* Unlock decoder before qbuf call:
   * qbuf will eventually block till frames
   * recycled, so frame_push_thread must
   * execute lock free..
   */
  GST_VIDEO_DECODER_STREAM_UNLOCK (decoder);

  /* push AU */
  qbuf = dqbuf;                 /* index from dqbuf */
  qbuf.bytesused = gstsize;     /* access unit size */

  /* access unit timestamp */
  if (GST_CLOCK_TIME_IS_VALID (frame->dts))
    GST_TIME_TO_TIMEVAL (frame->dts, qbuf.timestamp);
  else
    GST_TIME_TO_TIMEVAL (frame->pts, qbuf.timestamp);

  if (v4l2_ioctl (dec->fd, VIDIOC_QBUF, &qbuf) < 0) {
    GST_VIDEO_DECODER_STREAM_LOCK (decoder);
    goto error_ioctl_enqueue;
  }
  GST_VIDEO_DECODER_STREAM_LOCK (decoder);
  if (qbuf.flags & V4L2_BUF_FLAG_ERROR) {
    /* either the decoded frame is corrupted (decoding error)
     * or it should not be displayed (e.g. "invisible" VP8 frame) */
    GST_VIDEO_CODEC_FRAME_SET_DECODE_ONLY (frame);
    gst_video_decoder_finish_frame (decoder, frame);
  }

  return ret;

  /* ERRORS */
error_ioctl_dequeue_out:
  {
    GST_ERROR_OBJECT (dec, "Dequeuing input failed err=%s", strerror (errno));
    return GST_FLOW_ERROR;
  }
error_ioctl_enqueue:
  {
    GST_ERROR_OBJECT (dec, "Enqueuing failed err=%s", strerror (errno));
    return GST_FLOW_ERROR;
  }
}

/* The thread is in charge of recycle the displayed frame to V4L2 */
static void
frame_recycle_thread (void *arg)
{
  GstVideoDecoder *decoder = (GstVideoDecoder *) arg;
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GstMemory *gmem = NULL;
  GstBufferPoolAcquireParams params = {.flags =
        GST_BUFFER_POOL_ACQUIRE_FLAG_NONE
  };
  GstV4L2DecDownstreamMeta *meta = NULL;
  gint fd = -1;
  gboolean found = FALSE;
  struct v4l2_buffer qbuf;
  GstBuffer *buffer;
  int i;
  int ret;

  ret = gst_buffer_pool_acquire_buffer (dec->downstream_pool, &buffer, &params);
  if (ret != GST_FLOW_OK) {     /* logically caused by STREAMOFF */
    GST_LOG_OBJECT (dec, "The pool is no more active %d", ret);
    goto out;
  }

  found = FALSE;
  for (i = 0; i < ARRAY_SIZE (dec->downstream_buffers); i++) {
    if (dec->downstream_buffers[i] == buffer) {
      found = TRUE;
      break;
    }
  }

  if (!found) {
    GST_LOG_OBJECT (dec, "Unexpected buffer from acquire_buffer, buffer %p",
        buffer);
    goto exit;                  /* Additional buffers are allocated, only focus on ours. FIXME: Is unref needed ? */
  }

  meta = GST_V4L2DEC_DOWNSTREAM_META_GET (buffer);
  if (meta) {
    meta->acquired = TRUE;
    GST_LOG_OBJECT (dec, "set acquire TRUE: dmabuf fd %d, buffer %p, index %d",
        fd, buffer, i);
  }

  gmem = gst_buffer_get_memory (buffer, 0);
  if ((gmem != NULL) && (gst_is_dmabuf_memory (gmem) == TRUE)) {
    fd = gst_dmabuf_memory_get_fd (gmem);
    GST_LOG_OBJECT (dec,
        "Recycle output buffer: dmabuf fd %d, buffer %p, index %d", fd, buffer,
        i);
  }
  gst_memory_unref (gmem);

  GST_LOG_OBJECT (dec, "Recycle buffer index %d", i);
  memset (&qbuf, 0, sizeof qbuf);
  qbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  qbuf.memory = V4L2_MEMORY_DMABUF;
  qbuf.index = i;
  qbuf.m.fd = fd;
  qbuf.bytesused = dec->size_image;
  qbuf.length = dec->size_image;

  GST_DEBUG_OBJECT (dec, "Queue output buffer %p, index %d", buffer, i);
  if (v4l2_ioctl (dec->fd, VIDIOC_QBUF, &qbuf) < 0) {
    GST_ERROR_OBJECT (dec, "Queuing output buffer failed err=%s",
        strerror (errno));
    goto out;
  }
exit:
  return;

out:
  GST_DEBUG_OBJECT (dec, "-->Leaving the frame recycle thread!");
  gst_task_pause (dec->frame_recycle_task);
  return;
}

/* The thread is in charge of retrieve the decoded frame from V4L2
 * and push it to the next pad */
static void
frame_push_thread (void *arg)
{
  GstVideoDecoder *decoder = (GstVideoDecoder *) arg;
  GstV4L2Dec *dec = GST_V4L2DEC (decoder);
  GstVideoInfo *info = &dec->output_state->info;
  GstFlowReturn ret = GST_FLOW_OK;
  GstVideoCodecFrame *frame;
  GstBuffer *output_buffer;
  GstVideoFrameFlags flags = 0;
  gint fd = -1;
  gint index = 0;

  if (dec->pool) {
    ret = gst_buffer_pool_acquire_buffer (dec->pool, &output_buffer, NULL);
    if (ret != GST_FLOW_OK)     /* logically caused by STREAMOFF */
      goto out;
  } else if (dec->downstream_pool) {
    struct v4l2_buffer dqbuf;
    GstV4L2DecDownstreamMeta *meta = NULL;
    GST_DEBUG_OBJECT (dec, "trying to dequeue frame");

    memset (&dqbuf, 0, sizeof dqbuf);
    dqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    dqbuf.memory = V4L2_MEMORY_DMABUF;
    GST_DEBUG_OBJECT (dec, "dequeue buffer (capture type)");
    if (v4l2_ioctl (dec->fd, VIDIOC_DQBUF, &dqbuf) < 0)
      goto out;                 /* STREAMOFF */

    GST_DEBUG_OBJECT (dec, "dequeue buffer (capture type) dqbuf.index %d",
        dqbuf.index);

    /* get back GstBuffer from V4L2 index */
    output_buffer = dec->downstream_buffers[dqbuf.index];
    fd = dqbuf.m.fd;
    index = dqbuf.index;

    meta = GST_V4L2DEC_DOWNSTREAM_META_GET (output_buffer);
    if (meta) {
      meta->acquired = FALSE;
      GST_LOG_OBJECT (dec,
          "set acquire FALSE: dmabuf fd %d, buffer %p, index %d", fd,
          output_buffer, index);
    }

    GST_BUFFER_TIMESTAMP (output_buffer) =
        GST_TIMEVAL_TO_TIME (dqbuf.timestamp);

    /* set top/bottom field first if v4l2_buffer has the information */
    if ((dqbuf.field == V4L2_FIELD_INTERLACED_TB)
        || (dqbuf.field == V4L2_FIELD_INTERLACED)) {
      GST_BUFFER_FLAG_SET (output_buffer, GST_VIDEO_BUFFER_FLAG_INTERLACED);
      GST_BUFFER_FLAG_SET (output_buffer, GST_VIDEO_BUFFER_FLAG_TFF);
    } else if (dqbuf.field == V4L2_FIELD_INTERLACED_BT) {
      GST_BUFFER_FLAG_SET (output_buffer, GST_VIDEO_BUFFER_FLAG_INTERLACED);
      GST_BUFFER_FLAG_UNSET (output_buffer, GST_VIDEO_BUFFER_FLAG_TFF);
    } else {
      /* per default, the frame is considered as progressive */
      GST_BUFFER_FLAG_UNSET (output_buffer, GST_VIDEO_BUFFER_FLAG_INTERLACED);
      GST_BUFFER_FLAG_UNSET (output_buffer, GST_VIDEO_BUFFER_FLAG_TFF);
    }
  }

  frame = gst_video_decoder_get_oldest_frame (decoder);
  if (frame == NULL) {
    if (dec->pool)
      gst_buffer_pool_release_buffer (dec->pool, output_buffer);
    goto exit;
  }

  /* buffer flags enhance the meta flags */
  if (GST_BUFFER_FLAG_IS_SET (output_buffer, GST_VIDEO_BUFFER_FLAG_INTERLACED))
    flags |= GST_VIDEO_FRAME_FLAG_INTERLACED;
  if (GST_BUFFER_FLAG_IS_SET (output_buffer, GST_VIDEO_BUFFER_FLAG_TFF))
    flags |= GST_VIDEO_FRAME_FLAG_TFF;

  /* Add alignment info */
  gst_buffer_add_video_meta_full (output_buffer,
      flags,
      GST_VIDEO_INFO_FORMAT (info),
      GST_VIDEO_INFO_WIDTH (info),
      GST_VIDEO_INFO_HEIGHT (info),
      GST_VIDEO_INFO_N_PLANES (info), info->offset, info->stride);

  frame->output_buffer = output_buffer;
  frame->pts = output_buffer->pts;

  /* Decrease the refcount of the frame so that the frame is released by the
   * gst_video_decoder_finish_frame function and so that the output buffer is
   * writable when it's pushed downstream */
  gst_video_codec_frame_unref (frame);

  gst_video_decoder_finish_frame (decoder, frame);
  GST_DEBUG_OBJECT (dec, "-->Frame pushed buffer %p", output_buffer);

exit:
  return;

out:
  GST_DEBUG_OBJECT (dec, "-->Leaving the frame push thread!");
  gst_task_pause (dec->frame_push_task);
  return;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    v4l2dec,
    "V4L2 decoder",
    plugin_init, VERSION, GST_LICENSE, GST_PACKAGE_NAME, GST_PACKAGE_ORIGIN);
