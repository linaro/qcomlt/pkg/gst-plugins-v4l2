/*
* Copyright (C) 2013 STMicroelectronics SA
*
* Author: <hugues.fruchet@st.com> for STMicroelectronics.
*/
#ifndef __GST_V4L2DEC_BUFFER_POOL_H__
#define __GST_V4L2DEC_BUFFER_POOL_H__

#include <gst/gst.h>

typedef struct _GstV4L2DecBufferPool GstV4L2DecBufferPool;
typedef struct _GstV4L2DecBufferPoolClass GstV4L2DecBufferPoolClass;
typedef struct _GstV4L2DecMeta GstV4L2DecMeta;

#include "gstv4l2dec.h"

G_BEGIN_DECLS
#define GST_TYPE_V4L2DEC_BUFFER_POOL      (gst_v4l2dec_buffer_pool_get_type())
#define GST_IS_V4L2DEC_BUFFER_POOL(obj)   (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_V4L2DEC_BUFFER_POOL))
#define GST_V4L2DEC_BUFFER_POOL(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_V4L2DEC_BUFFER_POOL, GstV4L2DecBufferPool))
#define GST_V4L2DEC_BUFFER_POOL_CAST(obj) ((GstV4L2DecBufferPool*)(obj))
    struct _GstV4L2DecBufferPool
{
  GstBufferPool parent;
  GstV4L2Dec *dec;
  GstAllocator *allocator;
  GstAllocationParams params;

  gint video_fd;
  guint num_buffers;            /* number of buffers we use */
  guint num_allocated;          /* number of buffers allocated by the driver */
  guint num_queued;             /* number of buffers queued in the driver */

  GstBuffer **buffers;
};

struct _GstV4L2DecBufferPoolClass
{
  GstBufferPoolClass parent_class;
};

struct _GstV4L2DecMeta
{
  GstMeta meta;
  gpointer mem;
  struct v4l2_buffer vbuffer;
};

GType gst_v4l2dec_meta_api_get_type (void);
const GstMetaInfo *gst_v4l2dec_meta_get_info (void);
#define GST_V4L2DEC_META_GET(buf) ((GstV4L2DecMeta *)gst_buffer_get_meta(buf,gst_v4l2dec_meta_api_get_type()))
#define GST_V4L2DEC_META_ADD(buf) ((GstV4L2DecMeta *)gst_buffer_add_meta(buf,gst_v4l2dec_meta_get_info(),NULL))

GType gst_v4l2dec_buffer_pool_get_type (void);

GstBufferPool *gst_v4l2dec_buffer_pool_new (GstV4L2Dec * dec, GstCaps * caps,
    guint max, guint size, GstVideoAlignment * align);

G_END_DECLS
#endif /*__GST_V4L2DEC_BUFFER_POOL_H__ */
