/*
* Copyright (C) 2013 STMicroelectronics SA
*
* Author: <hugues.fruchet@st.com> for STMicroelectronics.
*/
#ifndef  GSTV4L2DEC_H
#define  GSTV4L2DEC_H

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideodecoder.h>
#include <gst/video/gstvideopool.h>
#include <gst/allocators/gstdmabuf.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <linux/videodev2.h>

#ifdef HAVE_LIBV4L2
#include <libv4l2.h>
#else
#define v4l2_fd_open(fd, flags) (fd)
#define v4l2_close    close
#define v4l2_dup      dup
#define v4l2_ioctl    ioctl
#define v4l2_read     read
#define v4l2_mmap     mmap
#define v4l2_munmap   munmap
#endif

/* Begin Declaration */
G_BEGIN_DECLS
#define GST_TYPE_V4L2DEC	(gst_v4l2dec_get_type())
#define GST_V4L2DEC(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_V4L2DEC, GstV4L2Dec))
#define GST_V4L2DEC_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_V4L2DEC, GstV4L2DecClass))
#define GST_IS_V4L2DEC(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_V4L2DEC))
#define GST_IS_V4L2DEC_CLASS(obj) \
	(G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_V4L2DEC))
typedef struct _GstV4L2Dec GstV4L2Dec;
typedef struct _GstV4L2DecClass GstV4L2DecClass;
typedef struct _GstV4L2DecDownstreamMeta GstV4L2DecDownstreamMeta;

#include <gstv4l2decbufferpool.h>

#define MAX_BUFFERS 30

struct _GstV4L2Dec
{
  GstVideoDecoder parent;

  gint fd;
  void **mmap_virtual_input;    /* Pointer tab of input AUs */
  gint *mmap_size_input;
  gint current_nb_buf_input;    /* used for input munmaping */

  /* Structure representing the state of an incoming or outgoing video stream
     for encoders and decoders. */
  GstVideoCodecState *input_state;
  GstVideoCodecState *output_state;

  gchar *format_in_to_str;
  gchar *format_out_to_str;

  GstCaps *caps_in;
  GstCaps *caps_out;

  GstBufferPool *pool;          /* Pool of output frames */
  GstBufferPool *downstream_pool;       /* Pool of output frames */

  gboolean output_setup;
  gboolean input_setup;

  gint width;
  gint height;
  __u32 streamformat;

  /* used for multi-threading */
  GstTask *frame_push_task;
  GRecMutex frame_push_task_mutex;

  GstTask *frame_recycle_task;
  GRecMutex frame_recycle_task_mutex;

  GstBuffer *codec_data;
  GstBuffer *header;

  unsigned char sps_pps_buf[100];
  unsigned int sps_pps_size;

  GstVideoAlignment align;
  gint size_image;

  GstBuffer *downstream_buffers[MAX_BUFFERS];
};

struct _GstV4L2DecClass
{
  GstVideoDecoderClass parent_class;
};

struct _GstV4L2DecDownstreamMeta
{
  GstMeta meta;
  gboolean acquired;
};

GType gst_v4l2dec_downstream_meta_api_get_type (void);
const GstMetaInfo *gst_v4l2dec_downstream_meta_get_info (void);
#define GST_V4L2DEC_DOWNSTREAM_META_GET(buf) ((GstV4L2DecDownstreamMeta *)gst_buffer_get_meta(buf,gst_v4l2dec_downstream_meta_api_get_type()))
#define GST_V4L2DEC_DOWNSTREAM_META_ADD(buf) ((GstV4L2DecDownstreamMeta *)gst_buffer_add_meta(buf,gst_v4l2dec_downstream_meta_get_info(),NULL))

GType gst_v4l2dec_get_type (void);


G_END_DECLS
#endif /* __GST_v4l2dec_H__ */
