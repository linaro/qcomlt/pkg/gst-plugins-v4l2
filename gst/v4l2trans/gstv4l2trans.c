/*
* Copyright (C) 2015 STMicroelectronics SA
*
* Author: <fabien.dessenne@st.com> for STMicroelectronics.
*
* Transform / Video filter Gstreamer plugin relying on the V4L2 interface
*
* Note that the variables and structures follow the V4L2 naming convention:
*  - "Output" refers to data & control from the upstream element.
*  - "Capture" refers to data & control to the downstream element.
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <gst/allocators/gstdmabuf.h>

#include "gstv4l2trans.h"
#include "gstv4l2capbufpool.h"
#include "gstv4l2outbufpool.h"

GST_DEBUG_CATEGORY (gst_v4l2trans_debug);
#define GST_CAT_DEFAULT gst_v4l2trans_debug

#define MAX_DEVICES          20 /* Max nb of candidate V4L2 M2M devices */
#define NB_BUF_NO_OUTPUT_POOL 1 /* Number of V4L2 buffers used when our output
                                   buffer pool is not used */
#define MIN_BUF_OUTPUT_POOL   2 /* Min number of buffers in the output buffer
                                   pool we own */

#define GST_VIDEO_SRC_FORMATS "{ RGB, NV12, BGRx, BGRA, RGB16, I420}"
#define GST_VIDEO_SINK_FORMATS "{ RGB, NV12, BGRx, BGRA, RGB16, I420}"

static GstStaticCaps gst_v4l2trans_src_format_caps =
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (GST_VIDEO_SRC_FORMATS) ";"
    GST_VIDEO_CAPS_MAKE_WITH_FEATURES ("ANY", GST_VIDEO_SRC_FORMATS));

static GstStaticCaps gst_v4l2trans_sink_format_caps =
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (GST_VIDEO_SINK_FORMATS) ";"
    GST_VIDEO_CAPS_MAKE_WITH_FEATURES ("ANY", GST_VIDEO_SINK_FORMATS));

#define parent_class gst_v4l2trans_parent_class
G_DEFINE_TYPE (GstV4L2Trans, gst_v4l2trans, GST_TYPE_VIDEO_FILTER);

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

gboolean plugin_init (GstPlugin * plugin);

static void gst_v4l2trans_finalize (GObject * object);
void gst_v4l2trans_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec);
void gst_v4l2trans_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec);

enum
{
  PROP_0,
  PROP_HFLIP,
  PROP_VFLIP
};

static GstCaps *
gst_v4l2trans_get_src_capslist (void)
{
  static GstCaps *caps = NULL;
  static volatile gsize inited = 0;

  if (g_once_init_enter (&inited)) {
    caps = gst_static_caps_get (&gst_v4l2trans_src_format_caps);
    g_once_init_leave (&inited, 1);
  }
  return caps;
}

static GstCaps *
gst_v4l2trans_get_sink_capslist (void)
{
  static GstCaps *caps = NULL;
  static volatile gsize inited = 0;

  if (g_once_init_enter (&inited)) {
    caps = gst_static_caps_get (&gst_v4l2trans_sink_format_caps);
    g_once_init_leave (&inited, 1);
  }
  return caps;
}

static GstPadTemplate *
gst_v4l2trans_src_template_factory (void)
{
  return gst_pad_template_new ("src", GST_PAD_SRC, GST_PAD_ALWAYS,
      gst_v4l2trans_get_src_capslist ());
}

static GstPadTemplate *
gst_v4l2trans_sink_template_factory (void)
{
  return gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
      gst_v4l2trans_get_sink_capslist ());
}

static void
gst_v4l2trans_finalize (GObject * object)
{
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static gboolean
gst_v4l2trans_open_device (GstV4L2Trans * trans)
{
  gint ret, ret2, c, libv4l2_fd, devnum, fd;
  gchar device_name[100];
  struct v4l2_capability cap;
  struct v4l2_format try_fmt_out, try_fmt_cap;
  gboolean found = FALSE;

  GST_DEBUG_OBJECT (trans, "Opening");

  memset (&try_fmt_out, 0, sizeof try_fmt_out);
  try_fmt_out.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  try_fmt_out.fmt.pix.pixelformat = V4L2_PIX_FMT_ABGR32;

  memset (&try_fmt_cap, 0, sizeof try_fmt_cap);
  try_fmt_cap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  try_fmt_cap.fmt.pix.pixelformat = V4L2_PIX_FMT_ABGR32;

  /* Search for a mem2mem device supporting ARGB capture & output */
  for (devnum = 0; devnum < MAX_DEVICES; devnum++) {
    snprintf (device_name, sizeof device_name, "/dev/video%d", devnum);
    fd = open (device_name, O_RDWR, 0);
    if (fd < 0)
      continue;

    libv4l2_fd = v4l2_fd_open (fd, V4L2_DISABLE_CONVERSION);
    if (libv4l2_fd != -1)
      fd = libv4l2_fd;

    ret = v4l2_ioctl (fd, VIDIOC_QUERYCAP, &cap);
    if (ret != 0) {
      v4l2_close (fd);
      continue;
    }

    /* Exclude drivers that are not M2M / Capture+Output */
    c = cap.capabilities;
    if (!(c & (V4L2_CAP_VIDEO_M2M | V4L2_CAP_VIDEO_M2M_MPLANE)) &&
        !(c & (V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_VIDEO_OUTPUT))) {
      v4l2_close (fd);
      continue;
    }

    /* Must support ARGB capture & output */
    ret = v4l2_ioctl (fd, VIDIOC_TRY_FMT, &try_fmt_out);
    ret2 = v4l2_ioctl (fd, VIDIOC_TRY_FMT, &try_fmt_cap);
    if ((ret < 0) || (ret2 < 0))
      continue;

    found = TRUE;
    break;
  }

  if (!found) {
    GST_ERROR_OBJECT (trans, "No device found");
    return FALSE;
  } else {
    trans->fd = fd;
    GST_INFO_OBJECT (trans, "Will use %s", device_name);
    return TRUE;
  }
}

static void
gst_v4l2trans_close_device (GstV4L2Trans * trans)
{
  GST_DEBUG_OBJECT (trans, "Closing");

  if (trans->fd != -1) {
    v4l2_close (trans->fd);
    trans->fd = -1;
  }
}

static gboolean
gst_v4l2trans_stop (GstBaseTransform * btrans)
{
  gint type, i;

  GstV4L2Trans *trans = GST_V4L2TRANS (btrans);
  GST_DEBUG_OBJECT (btrans, "Stopping");

  /* Close & free output resources (no bufferpool mode) */
  if (trans->out_no_pool.setup) {
    if (trans->fd != -1) {
      type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
      if (v4l2_ioctl (trans->fd, VIDIOC_STREAMOFF, &type) < 0)
        GST_WARNING_OBJECT (btrans, "Unable to stop stream on output");
    }

    if (trans->out_no_pool.mmap_virt) {
      for (i = 0; i < trans->out_no_pool.current_nb_buf; i++)
        v4l2_munmap (trans->out_no_pool.mmap_virt[i],
            trans->out_no_pool.mmap_size[i]);

      g_free (trans->out_no_pool.mmap_virt);
      g_free (trans->out_no_pool.mmap_size);
    }
  }

  /* Close & free capture resources */
  if (trans->fd != -1) {
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (v4l2_ioctl (trans->fd, VIDIOC_STREAMOFF, &type) < 0)
      GST_WARNING_OBJECT (btrans, "Unable to stop stream on capture");
  }

  /* Release buffer pool */
  if (trans->capture_down_pool)
    gst_object_unref (trans->capture_down_pool);

  return TRUE;
}

static gint
gst_v4l2trans_fmt_gst_to_v4l2 (GstVideoInfo * info)
{
  switch (GST_VIDEO_INFO_FORMAT (info)) {
    case GST_VIDEO_FORMAT_RGB:
      return V4L2_PIX_FMT_RGB24;
    case GST_VIDEO_FORMAT_I420:
      return V4L2_PIX_FMT_YUV420;
    case GST_VIDEO_FORMAT_NV12:
      return V4L2_PIX_FMT_NV12;
    case GST_VIDEO_FORMAT_RGB16:
      return V4L2_PIX_FMT_RGB565;
#ifdef V4L2_PIX_FMT_XBGR32
    case GST_VIDEO_FORMAT_BGRx:
      return V4L2_PIX_FMT_XBGR32;       /* Actually refers to B-G-R-X */
#endif
    case GST_VIDEO_FORMAT_BGRA:
      return V4L2_PIX_FMT_ABGR32;       /* Actually refers to B-G-R-A */
    default:
      return 0;
  }
}

static gint
gst_v4l2trans_align_width (GstVideoInfo * info)
{
  if (info->finfo->format == GST_VIDEO_FORMAT_NV12 ||
      info->finfo->format == GST_VIDEO_FORMAT_I420)
    return (GST_VIDEO_INFO_PLANE_STRIDE (info, 0));
  else
    return (GST_VIDEO_INFO_WIDTH (info));
}

static gint
gst_v4l2trans_align_height (GstVideoInfo * info)
{
  if (info->finfo->format == GST_VIDEO_FORMAT_NV12 ||
      info->finfo->format == GST_VIDEO_FORMAT_I420)
    return (GST_VIDEO_INFO_PLANE_OFFSET (info, 1) /
        GST_VIDEO_INFO_PLANE_STRIDE (info, 0));
  else
    return (GST_VIDEO_INFO_HEIGHT (info));
}

static gboolean
gst_v4l2trans_capture_setup (GstV4L2Trans * trans, GstVideoInfo * info,
    guint count, guint memory)
{
  struct v4l2_format s_fmt;
  struct v4l2_requestbuffers reqbuf;

  /* Stop stream if already started */
  if (trans->capture_start) {
    gint type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (v4l2_ioctl (trans->fd, VIDIOC_STREAMOFF, &type) < 0) {
      GST_WARNING_OBJECT (trans, "Unable to stop stream on capture");
      return FALSE;
    }
  }
  trans->capture_start = FALSE;

  /* Set format */
  memset (&s_fmt, 0, sizeof s_fmt);
  s_fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  s_fmt.fmt.pix.width = gst_v4l2trans_align_width (info);
  s_fmt.fmt.pix.height = gst_v4l2trans_align_height (info);
  s_fmt.fmt.pix.pixelformat = gst_v4l2trans_fmt_gst_to_v4l2 (info);
  s_fmt.fmt.pix.sizeimage = GST_VIDEO_INFO_SIZE (info);

  if (v4l2_ioctl (trans->fd, VIDIOC_S_FMT, &s_fmt) < 0) {
    GST_ERROR_OBJECT (trans, "Capture VIDIOC_S_FMT failed");
    return FALSE;
  }

  /* Request capture buffers  */
  memset (&reqbuf, 0, sizeof reqbuf);
  reqbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  reqbuf.count = count;
  reqbuf.memory = memory;

  if (v4l2_ioctl (trans->fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
    GST_ERROR_OBJECT (trans, "Capture VIDIOC_REQBUFS failed");
    return FALSE;
  }

  /* Clear V4L2 capture fd table */
  memset (&trans->capture_v4l2_fd, -1, sizeof trans->capture_v4l2_fd);

  return TRUE;
}

static gboolean
gst_v4l2trans_decide_allocation (GstBaseTransform * btrans, GstQuery * query)
{
  GstV4L2Trans *trans = GST_V4L2TRANS (btrans);
  GstCaps *outcaps;
  GstVideoInfo info;
  GstAllocator *allocator;
  GstAllocationParams params;
  GstBufferPool *pool;
  guint allocator_nb, i, size, minbuf, maxbuf;
  gboolean dmabuf_found = FALSE;
  gboolean active_pool = FALSE;

  /* Get caps and VideoInfo */
  gst_query_parse_allocation (query, &outcaps, NULL);
  gst_video_info_from_caps (&info, outcaps);

  GST_DEBUG_OBJECT (btrans, "Deciding for outcaps %" GST_PTR_FORMAT, outcaps);

  /* parse the allocators searching for a dmabuf allocator */
  allocator_nb = gst_query_get_n_allocation_params (query);
  GST_DEBUG_OBJECT (btrans, "Got %d allocator(s)", allocator_nb);

  for (i = 0; i < allocator_nb; i++) {
    gst_query_parse_nth_allocation_param (query, i, &allocator, &params);

    if (!allocator)
      continue;

    GST_DEBUG_OBJECT (btrans, "Allocator %d = %s", i, allocator->mem_type);

    /* Looking for a DMABUF allocator */
    if (!g_strcmp0 (allocator->mem_type, GST_ALLOCATOR_DMABUF)) {
      if (trans->capture_down_pool &&
          gst_buffer_pool_is_active (trans->capture_down_pool)) {
        /* Pool was already active */
        GST_DEBUG_OBJECT (btrans, "Pool %s already active",
            trans->capture_down_pool->object.name);
        active_pool = TRUE;
      }

      if (!active_pool)
        /* Select the DMABUF allocator */
        gst_query_set_nth_allocation_param (query, 0, allocator, &params);

      /* Configure pool */
      gst_query_parse_nth_allocation_pool (query, 0, &pool, &size,
          &minbuf, &maxbuf);
      GST_DEBUG_OBJECT (btrans, "Found downstream DMABUF pool 0x%p "
          "with min=%d max=%d ", pool, minbuf, maxbuf);

      /* Set the nb of buffers of the downstream pool */
      minbuf = maxbuf = NB_BUF_CAPTURE_POOL;
      gst_query_add_allocation_pool (query, pool, size, minbuf, maxbuf);
      gst_query_set_nth_allocation_pool (query, 0, pool, size, minbuf, maxbuf);
      GST_DEBUG_OBJECT (btrans, "Set downstream DMABUFpool %p to min=%d max=%d",
          pool, minbuf, maxbuf);

      /* Configure V4L2 format and request DMABUF buffers : one V4L2 buffer for
       * each buffer from the pool.
       * Here (MEMORY_DMABUF) V4L2 does not allocate buffers memory */
      if (!gst_v4l2trans_capture_setup (trans, &info, NB_BUF_CAPTURE_POOL,
              V4L2_MEMORY_DMABUF))
        return FALSE;

      GST_DEBUG_OBJECT (btrans, "Capture for V4L2 configured");

      /* Set downstream pool reference */
      if (active_pool)
        gst_object_unref (trans->capture_down_pool);
      trans->capture_down_pool = pool;
      dmabuf_found = TRUE;
      break;
    }
    gst_object_unref (allocator);
  }

  if (!dmabuf_found) {
    GstBufferPool *pool;
    GstAllocator *allocator;
    GstStructure *config;
    GstAllocationParams params;

    /* Allocate a pool */
    GST_DEBUG_OBJECT (btrans, "No DMABUF allocator found: creating pool");

    pool = gst_v4l2_cap_buf_pool_new (trans, outcaps,
        GST_VIDEO_INFO_SIZE (&info), NB_BUF_CAPTURE_POOL);

    gst_query_add_allocation_pool (query, pool, GST_VIDEO_INFO_SIZE (&info),
        0, NB_BUF_CAPTURE_POOL);
    config = gst_buffer_pool_get_config (pool);
    gst_buffer_pool_config_get_allocator (config, &allocator, &params);
    gst_query_add_allocation_param (query, allocator, &params);

    gst_object_unref (pool);

    /* Configure V4L2 format and request MMAP buffers : one V4L2 buffer for
     * each buffer from the pool.
     * Here (MEMORY_MMAP) V4L2 actually allocate buffers memory */
    if (!gst_v4l2trans_capture_setup (trans, &info, NB_BUF_CAPTURE_POOL,
            V4L2_MEMORY_MMAP))
      return FALSE;

    GST_DEBUG_OBJECT (btrans, "Capture for V4L2 configured");

    trans->capture_pool = pool;
  }

  return GST_BASE_TRANSFORM_CLASS (parent_class)->decide_allocation (btrans,
      query);
}

static gboolean
gst_v4l2trans_propose_allocation (GstBaseTransform * btrans,
    GstQuery * decide_query, GstQuery * query)
{
  GstV4L2Trans *trans = GST_V4L2TRANS (btrans);
  GstBufferPool *pool;
  GstStructure *config;
  GstCaps *caps;
  GstAllocator *allocator;
  GstAllocationParams params;
  GstCaps *pcaps;
  GstVideoInfo info;
  guint size;
  gboolean need_pool;

  GST_DEBUG_OBJECT (trans, "Proposing output buffer pool");

  if (decide_query == NULL)
    goto out;

  gst_query_parse_allocation (query, &caps, &need_pool);

  if (caps == NULL)
    goto no_caps;

  pool = trans->output_pool;

  if (pool != NULL) {
    gst_object_ref (pool);

    /* we had a pool, check caps */
    config = gst_buffer_pool_get_config (pool);
    gst_buffer_pool_config_get_params (config, &pcaps, &size, NULL, NULL);
    GST_DEBUG_OBJECT (trans, "We had a pool with caps %" GST_PTR_FORMAT, pcaps);

    if (!gst_caps_is_equal (caps, pcaps)) {
      /* different caps, we can't use this pool */
      gst_object_unref (pool);
      GST_DEBUG_OBJECT (trans, "Pool has different caps");
      pool = NULL;
    } else {
      GST_DEBUG_OBJECT (trans, "Pool has same caps, proposing again");

      if (!gst_video_info_from_caps (&info, caps))
        goto invalid_caps;

      /* Add buffer pool and VIDEO META */
      gst_query_add_allocation_pool (query, pool, info.size,
          MIN_BUF_OUTPUT_POOL, 0);
      gst_query_add_allocation_meta (query, GST_VIDEO_META_API_TYPE, NULL);
      gst_object_unref (pool);

      /* Add DMA-BUF allocator */
      allocator = gst_dmabuf_allocator_new ();
      gst_allocation_params_init (&params);
      gst_query_add_allocation_param (query, allocator, &params);
      gst_object_unref (allocator);
    }
    gst_structure_free (config);
  }

  if (pool == NULL && need_pool) {
    if (!gst_video_info_from_caps (&info, caps))
      goto invalid_caps;

    GST_DEBUG_OBJECT (trans, "Creating output buffer pool");

    pool = gst_v4l2_out_buf_pool_new (trans);
    if (pool == NULL)
      goto error_new_pool;

    trans->output_pool = pool;

    /* Add buffer pool and VIDEO META */
    gst_query_add_allocation_pool (query, pool, info.size,
        MIN_BUF_OUTPUT_POOL, 0);
    gst_query_add_allocation_meta (query, GST_VIDEO_META_API_TYPE, NULL);
    if (pool)
      gst_object_unref (pool);

    /* Add DMA-BUF allocator */
    allocator = gst_dmabuf_allocator_new ();
    gst_allocation_params_init (&params);
    gst_query_add_allocation_param (query, allocator, &params);
    gst_object_unref (allocator);
  }

out:
  return GST_BASE_TRANSFORM_CLASS (parent_class)->propose_allocation (btrans,
      decide_query, query);

no_caps:
  {
    GST_DEBUG_OBJECT (trans, "No caps specified");
    return FALSE;
  }
invalid_caps:
  {
    GST_DEBUG_OBJECT (trans, "Invalid caps specified");
    return FALSE;
  }
error_new_pool:
  {
    GST_ERROR_OBJECT (trans, "Unable to construct a new buffer pool");
    return GST_FLOW_ERROR;
  }
}

static GstCaps *
gst_v4l2trans_transform_caps (GstBaseTransform * btrans,
    GstPadDirection direction, GstCaps * caps, GstCaps * filter)
{
  GstCaps *tmp, *result, *res;
  GstStructure *st;
  GstCapsFeatures *f;
  gint i, n;

  GST_DEBUG_OBJECT (btrans,
      "Transforming caps %" GST_PTR_FORMAT " in direction %s", caps,
      (direction == GST_PAD_SINK) ? "sink" : "src");

  res = gst_caps_new_empty ();
  n = gst_caps_get_size (caps);
  for (i = 0; i < n; i++) {
    st = gst_caps_get_structure (caps, i);
    f = gst_caps_get_features (caps, i);

    /* If this is already expressed by the existing caps skip this structure */
    if (i > 0 && gst_caps_is_subset_structure_full (res, st, f))
      continue;

    st = gst_structure_copy (st);

    /* remove the informations (format, width, height...) for the cases
     * when we can actually transform */
    if (!gst_caps_features_is_any (f)
        && gst_caps_features_is_equal (f,
            GST_CAPS_FEATURES_MEMORY_SYSTEM_MEMORY)) {
      gst_structure_remove_fields (st, "width", "height", NULL);
      gst_structure_remove_fields (st, "format", "pixel-aspect-ratio", NULL);
    }
    gst_caps_append_structure_full (res, st, gst_caps_features_copy (f));
  }

  if (filter) {
    tmp = gst_caps_intersect_full (filter, res, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (res);
    res = tmp;
  }
  result = res;

  GST_DEBUG_OBJECT (btrans, "Transformed %" GST_PTR_FORMAT " into %"
      GST_PTR_FORMAT, caps, result);

  return result;
}

/* The following function is copied from gst-plugins-base/gst/videoscale/
 * gstvideoscale.c (file under the terms of the GNU Library General Public
 * License version 2 or later).
 * It fixates the width, height and pixel-aspect-ratio capabilities for the
 * scaling transform.
 */
static GstCaps *
gst_video_scale_fixate_caps (GstBaseTransform * base, GstPadDirection direction,
    GstCaps * caps, GstCaps * othercaps)
{
  GstStructure *ins, *outs;
  const GValue *from_par, *to_par;
  GValue fpar = { 0, }, tpar = {
  0,};

  othercaps = gst_caps_truncate (othercaps);
  othercaps = gst_caps_make_writable (othercaps);

  GST_DEBUG_OBJECT (base, "Trying to fixate othercaps %" GST_PTR_FORMAT
      " based on caps %" GST_PTR_FORMAT, othercaps, caps);

  ins = gst_caps_get_structure (caps, 0);
  outs = gst_caps_get_structure (othercaps, 0);

  from_par = gst_structure_get_value (ins, "pixel-aspect-ratio");
  to_par = gst_structure_get_value (outs, "pixel-aspect-ratio");

  /* If we're fixating from the sinkpad we always set the PAR and
   * assume that missing PAR on the sinkpad means 1/1 and
   * missing PAR on the srcpad means undefined
   */
  if (direction == GST_PAD_SINK) {
    if (!from_par) {
      g_value_init (&fpar, GST_TYPE_FRACTION);
      gst_value_set_fraction (&fpar, 1, 1);
      from_par = &fpar;
    }
    if (!to_par) {
      g_value_init (&tpar, GST_TYPE_FRACTION_RANGE);
      gst_value_set_fraction_range_full (&tpar, 1, G_MAXINT, G_MAXINT, 1);
      to_par = &tpar;
    }
  } else {
    if (!to_par) {
      g_value_init (&tpar, GST_TYPE_FRACTION);
      gst_value_set_fraction (&tpar, 1, 1);
      to_par = &tpar;

      gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION, 1, 1,
          NULL);
    }
    if (!from_par) {
      g_value_init (&fpar, GST_TYPE_FRACTION);
      gst_value_set_fraction (&fpar, 1, 1);
      from_par = &fpar;
    }
  }

  /* we have both PAR but they might not be fixated */
  {
    gint from_w, from_h, from_par_n, from_par_d, to_par_n, to_par_d;
    gint w = 0, h = 0;
    gint from_dar_n, from_dar_d;
    gint num, den;

    /* from_par should be fixed */
    g_return_val_if_fail (gst_value_is_fixed (from_par), othercaps);

    from_par_n = gst_value_get_fraction_numerator (from_par);
    from_par_d = gst_value_get_fraction_denominator (from_par);

    gst_structure_get_int (ins, "width", &from_w);
    gst_structure_get_int (ins, "height", &from_h);

    gst_structure_get_int (outs, "width", &w);
    gst_structure_get_int (outs, "height", &h);

    /* if both width and height are already fixed, we can't do anything
     * about it anymore */
    if (w && h) {
      guint n, d;

      GST_DEBUG_OBJECT (base, "Dimensions already set to %dx%d, not fixating",
          w, h);
      if (!gst_value_is_fixed (to_par)) {
        if (gst_video_calculate_display_ratio (&n, &d, from_w, from_h,
                from_par_n, from_par_d, w, h)) {
          GST_DEBUG_OBJECT (base, "Fixating to_par to %dx%d", n, d);
          if (gst_structure_has_field (outs, "pixel-aspect-ratio"))
            gst_structure_fixate_field_nearest_fraction (outs,
                "pixel-aspect-ratio", n, d);
          else if (n != d)
            gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION,
                n, d, NULL);
        }
      }
      goto done;
    }

    /* Calculate input DAR */
    if (!gst_util_fraction_multiply (from_w, from_h, from_par_n, from_par_d,
            &from_dar_n, &from_dar_d)) {
      GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
          ("Error calculating the output scaled size - integer overflow"));
      goto done;
    }

    GST_DEBUG_OBJECT (base, "Input DAR is %d/%d", from_dar_n, from_dar_d);

    /* If either width or height are fixed there's not much we
     * can do either except choosing a height or width and PAR
     * that matches the DAR as good as possible
     */
    if (h) {
      GstStructure *tmp;
      gint set_w, set_par_n, set_par_d;

      GST_DEBUG_OBJECT (base, "Height is fixed (%d)", h);

      /* If the PAR is fixed too, there's not much to do
       * except choosing the width that is nearest to the
       * width with the same DAR */
      if (gst_value_is_fixed (to_par)) {
        to_par_n = gst_value_get_fraction_numerator (to_par);
        to_par_d = gst_value_get_fraction_denominator (to_par);

        GST_DEBUG_OBJECT (base, "PAR is fixed %d/%d", to_par_n, to_par_d);

        if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, to_par_d,
                to_par_n, &num, &den)) {
          GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
              ("Error calculating the output scaled size - integer overflow"));
          goto done;
        }

        w = (guint) gst_util_uint64_scale_int (h, num, den);
        gst_structure_fixate_field_nearest_int (outs, "width", w);

        goto done;
      }

      /* The PAR is not fixed and it's quite likely that we can set
       * an arbitrary PAR. */

      /* Check if we can keep the input width */
      tmp = gst_structure_copy (outs);
      gst_structure_fixate_field_nearest_int (tmp, "width", from_w);
      gst_structure_get_int (tmp, "width", &set_w);

      /* Might have failed but try to keep the DAR nonetheless by
       * adjusting the PAR */
      if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, h, set_w,
              &to_par_n, &to_par_d)) {
        GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
            ("Error calculating the output scaled size - integer overflow"));
        gst_structure_free (tmp);
        goto done;
      }

      if (!gst_structure_has_field (tmp, "pixel-aspect-ratio"))
        gst_structure_set_value (tmp, "pixel-aspect-ratio", to_par);
      gst_structure_fixate_field_nearest_fraction (tmp, "pixel-aspect-ratio",
          to_par_n, to_par_d);
      gst_structure_get_fraction (tmp, "pixel-aspect-ratio", &set_par_n,
          &set_par_d);
      gst_structure_free (tmp);

      /* Check if the adjusted PAR is accepted */
      if (set_par_n == to_par_n && set_par_d == to_par_d) {
        if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
            set_par_n != set_par_d)
          gst_structure_set (outs, "width", G_TYPE_INT, set_w,
              "pixel-aspect-ratio", GST_TYPE_FRACTION, set_par_n, set_par_d,
              NULL);
        goto done;
      }

      /* Otherwise scale the width to the new PAR and check if the
       * adjusted with is accepted. If all that fails we can't keep
       * the DAR */
      if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, set_par_d,
              set_par_n, &num, &den)) {
        GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
            ("Error calculating the output scaled size - integer overflow"));
        goto done;
      }

      w = (guint) gst_util_uint64_scale_int (h, num, den);
      gst_structure_fixate_field_nearest_int (outs, "width", w);
      if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
          set_par_n != set_par_d)
        gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION,
            set_par_n, set_par_d, NULL);

      goto done;
    } else if (w) {
      GstStructure *tmp;
      gint set_h, set_par_n, set_par_d;

      GST_DEBUG_OBJECT (base, "Width is fixed (%d)", w);

      /* If the PAR is fixed too, there's not much to do
       * except choosing the height that is nearest to the
       * height with the same DAR */
      if (gst_value_is_fixed (to_par)) {
        to_par_n = gst_value_get_fraction_numerator (to_par);
        to_par_d = gst_value_get_fraction_denominator (to_par);

        GST_DEBUG_OBJECT (base, "PAR is fixed %d/%d", to_par_n, to_par_d);

        if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, to_par_d,
                to_par_n, &num, &den)) {
          GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
              ("Error calculating the output scaled size - integer overflow"));
          goto done;
        }

        h = (guint) gst_util_uint64_scale_int (w, den, num);
        gst_structure_fixate_field_nearest_int (outs, "height", h);

        goto done;
      }

      /* The PAR is not fixed and it's quite likely that we can set
       * an arbitrary PAR. */

      /* Check if we can keep the input height */
      tmp = gst_structure_copy (outs);
      gst_structure_fixate_field_nearest_int (tmp, "height", from_h);
      gst_structure_get_int (tmp, "height", &set_h);

      /* Might have failed but try to keep the DAR nonetheless by
       * adjusting the PAR */
      if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, set_h, w,
              &to_par_n, &to_par_d)) {
        GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
            ("Error calculating the output scaled size - integer overflow"));
        gst_structure_free (tmp);
        goto done;
      }
      if (!gst_structure_has_field (tmp, "pixel-aspect-ratio"))
        gst_structure_set_value (tmp, "pixel-aspect-ratio", to_par);
      gst_structure_fixate_field_nearest_fraction (tmp, "pixel-aspect-ratio",
          to_par_n, to_par_d);
      gst_structure_get_fraction (tmp, "pixel-aspect-ratio", &set_par_n,
          &set_par_d);
      gst_structure_free (tmp);

      /* Check if the adjusted PAR is accepted */
      if (set_par_n == to_par_n && set_par_d == to_par_d) {
        if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
            set_par_n != set_par_d)
          gst_structure_set (outs, "height", G_TYPE_INT, set_h,
              "pixel-aspect-ratio", GST_TYPE_FRACTION, set_par_n, set_par_d,
              NULL);
        goto done;
      }

      /* Otherwise scale the height to the new PAR and check if the
       * adjusted with is accepted. If all that fails we can't keep
       * the DAR */
      if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, set_par_d,
              set_par_n, &num, &den)) {
        GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
            ("Error calculating the output scaled size - integer overflow"));
        goto done;
      }

      h = (guint) gst_util_uint64_scale_int (w, den, num);
      gst_structure_fixate_field_nearest_int (outs, "height", h);
      if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
          set_par_n != set_par_d)
        gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION,
            set_par_n, set_par_d, NULL);

      goto done;
    } else if (gst_value_is_fixed (to_par)) {
      GstStructure *tmp;
      gint set_h, set_w, f_h, f_w;

      to_par_n = gst_value_get_fraction_numerator (to_par);
      to_par_d = gst_value_get_fraction_denominator (to_par);

      /* Calculate scale factor for the PAR change */
      if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, to_par_n,
              to_par_d, &num, &den)) {
        GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
            ("Error calculating the output scaled size - integer overflow"));
        goto done;
      }

      /* Try to keep the input height (because of interlacing) */
      tmp = gst_structure_copy (outs);
      gst_structure_fixate_field_nearest_int (tmp, "height", from_h);
      gst_structure_get_int (tmp, "height", &set_h);

      /* This might have failed but try to scale the width
       * to keep the DAR nonetheless */
      w = (guint) gst_util_uint64_scale_int (set_h, num, den);
      gst_structure_fixate_field_nearest_int (tmp, "width", w);
      gst_structure_get_int (tmp, "width", &set_w);
      gst_structure_free (tmp);

      /* We kept the DAR and the height is nearest to the original height */
      if (set_w == w) {
        gst_structure_set (outs, "width", G_TYPE_INT, set_w, "height",
            G_TYPE_INT, set_h, NULL);
        goto done;
      }

      f_h = set_h;
      f_w = set_w;

      /* If the former failed, try to keep the input width at least */
      tmp = gst_structure_copy (outs);
      gst_structure_fixate_field_nearest_int (tmp, "width", from_w);
      gst_structure_get_int (tmp, "width", &set_w);

      /* This might have failed but try to scale the width
       * to keep the DAR nonetheless */
      h = (guint) gst_util_uint64_scale_int (set_w, den, num);
      gst_structure_fixate_field_nearest_int (tmp, "height", h);
      gst_structure_get_int (tmp, "height", &set_h);
      gst_structure_free (tmp);

      /* We kept the DAR and the width is nearest to the original width */
      if (set_h == h) {
        gst_structure_set (outs, "width", G_TYPE_INT, set_w, "height",
            G_TYPE_INT, set_h, NULL);
        goto done;
      }

      /* If all this failed, keep the height that was nearest to the orignal
       * height and the nearest possible width. This changes the DAR but
       * there's not much else to do here.
       */
      gst_structure_set (outs, "width", G_TYPE_INT, f_w, "height", G_TYPE_INT,
          f_h, NULL);
      goto done;
    } else {
      GstStructure *tmp;
      gint set_h, set_w, set_par_n, set_par_d, tmp2;

      /* width, height and PAR are not fixed but passthrough is not possible */

      /* First try to keep the height and width as good as possible
       * and scale PAR */
      tmp = gst_structure_copy (outs);
      gst_structure_fixate_field_nearest_int (tmp, "height", from_h);
      gst_structure_get_int (tmp, "height", &set_h);
      gst_structure_fixate_field_nearest_int (tmp, "width", from_w);
      gst_structure_get_int (tmp, "width", &set_w);

      if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, set_h, set_w,
              &to_par_n, &to_par_d)) {
        GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
            ("Error calculating the output scaled size - integer overflow"));
        goto done;
      }

      if (!gst_structure_has_field (tmp, "pixel-aspect-ratio"))
        gst_structure_set_value (tmp, "pixel-aspect-ratio", to_par);
      gst_structure_fixate_field_nearest_fraction (tmp, "pixel-aspect-ratio",
          to_par_n, to_par_d);
      gst_structure_get_fraction (tmp, "pixel-aspect-ratio", &set_par_n,
          &set_par_d);
      gst_structure_free (tmp);

      if (set_par_n == to_par_n && set_par_d == to_par_d) {
        gst_structure_set (outs, "width", G_TYPE_INT, set_w, "height",
            G_TYPE_INT, set_h, NULL);

        if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
            set_par_n != set_par_d)
          gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION,
              set_par_n, set_par_d, NULL);
        goto done;
      }

      /* Otherwise try to scale width to keep the DAR with the set
       * PAR and height */
      if (!gst_util_fraction_multiply (from_dar_n, from_dar_d, set_par_d,
              set_par_n, &num, &den)) {
        GST_ELEMENT_ERROR (base, CORE, NEGOTIATION, (NULL),
            ("Error calculating the output scaled size - integer overflow"));
        goto done;
      }

      w = (guint) gst_util_uint64_scale_int (set_h, num, den);
      tmp = gst_structure_copy (outs);
      gst_structure_fixate_field_nearest_int (tmp, "width", w);
      gst_structure_get_int (tmp, "width", &tmp2);
      gst_structure_free (tmp);

      if (tmp2 == w) {
        gst_structure_set (outs, "width", G_TYPE_INT, tmp2, "height",
            G_TYPE_INT, set_h, NULL);
        if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
            set_par_n != set_par_d)
          gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION,
              set_par_n, set_par_d, NULL);
        goto done;
      }

      /* ... or try the same with the height */
      h = (guint) gst_util_uint64_scale_int (set_w, den, num);
      tmp = gst_structure_copy (outs);
      gst_structure_fixate_field_nearest_int (tmp, "height", h);
      gst_structure_get_int (tmp, "height", &tmp2);
      gst_structure_free (tmp);

      if (tmp2 == h) {
        gst_structure_set (outs, "width", G_TYPE_INT, set_w, "height",
            G_TYPE_INT, tmp2, NULL);
        if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
            set_par_n != set_par_d)
          gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION,
              set_par_n, set_par_d, NULL);
        goto done;
      }

      /* If all fails we can't keep the DAR and take the nearest values
       * for everything from the first try */
      gst_structure_set (outs, "width", G_TYPE_INT, set_w, "height",
          G_TYPE_INT, set_h, NULL);
      if (gst_structure_has_field (outs, "pixel-aspect-ratio") ||
          set_par_n != set_par_d)
        gst_structure_set (outs, "pixel-aspect-ratio", GST_TYPE_FRACTION,
            set_par_n, set_par_d, NULL);
    }
  }

done:
  GST_DEBUG_OBJECT (base, "Fixated othercaps to %" GST_PTR_FORMAT, othercaps);

  if (from_par == &fpar)
    g_value_unset (&fpar);
  if (to_par == &tpar)
    g_value_unset (&tpar);

  return othercaps;
}

static GstCaps *
gst_v4l2trans_fixate_caps (GstBaseTransform * btrans,
    GstPadDirection direction, GstCaps * caps, GstCaps * othercaps)
{
  GstCaps *prefcaps, *orderedcaps;
  const GValue *par, *other_par;

  /* fixate the scaling caps (width, height, pixel-aspect-ratio) */
  othercaps = gst_video_scale_fixate_caps (btrans, direction, caps, othercaps);

  /* if needed copy PAR from caps to other caps */
  par = gst_structure_get_value (gst_caps_get_structure (caps, 0),
      "pixel-aspect-ratio");
  other_par = gst_structure_get_value (gst_caps_get_structure (othercaps, 0),
      "pixel-aspect-ratio");
  if (par && !other_par)
    gst_structure_set_value (gst_caps_get_structure (othercaps, 0),
        "pixel-aspect-ratio", par);

  /* fixate any remaining field (e.g. format).
   * Select the format according to the format ordered caps list */
  if (direction == GST_PAD_SRC)
    prefcaps = gst_v4l2trans_get_sink_capslist ();
  else
    prefcaps = gst_v4l2trans_get_src_capslist ();

  orderedcaps = gst_caps_intersect_full (prefcaps, othercaps,
      GST_CAPS_INTERSECT_FIRST);
  gst_caps_unref (othercaps);
  othercaps = gst_caps_fixate (orderedcaps);

  GST_DEBUG_OBJECT (btrans, "Fixated othercaps to %" GST_PTR_FORMAT, othercaps);

  return othercaps;
}

static GstFlowReturn
gst_v4l2trans_setup_output_no_pool (GstV4L2Trans * trans, GstVideoFrame * in)
{
  struct v4l2_format s_fmt;
  struct v4l2_crop crop;
  struct v4l2_cropcap cropcap;
  struct v4l2_requestbuffers reqbuf;
  struct v4l2_buffer querybuf, qbuf;
  struct v4l2_control ctrl;
  gint width, height, crop_width, crop_height, i, type;
  void *ptr;

  /* Set format */
  width = gst_v4l2trans_align_width (&in->info);
  height = gst_v4l2trans_align_height (&in->info);
  crop_width = GST_VIDEO_INFO_WIDTH (&in->info);
  crop_height = GST_VIDEO_INFO_HEIGHT (&in->info);

  memset (&s_fmt, 0, sizeof s_fmt);
  s_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  s_fmt.fmt.pix.width = width;
  s_fmt.fmt.pix.height = height;
  s_fmt.fmt.pix.sizeimage = GST_VIDEO_INFO_SIZE (&in->info);
  s_fmt.fmt.pix.pixelformat = gst_v4l2trans_fmt_gst_to_v4l2 (&in->info);

  /* Format */
  if (v4l2_ioctl (trans->fd, VIDIOC_S_FMT, &s_fmt) < 0) {
    GST_ERROR_OBJECT (trans, "Output VIDIOC_S_FMT failed");
    return GST_FLOW_ERROR;
  }

  /* Flip */
  memset (&ctrl, 0, sizeof ctrl);
  ctrl.id = V4L2_CID_HFLIP;
  ctrl.value = trans->hflip;
  if (v4l2_ioctl (trans->fd, VIDIOC_S_CTRL, &ctrl) < 0) {
    GST_ERROR_OBJECT (trans, "Output VIDIOC_S_CTRL failed");
    return GST_FLOW_ERROR;
  }

  ctrl.id = V4L2_CID_VFLIP;
  ctrl.value = trans->vflip;
  if (v4l2_ioctl (trans->fd, VIDIOC_S_CTRL, &ctrl) < 0) {
    GST_ERROR_OBJECT (trans, "Output VIDIOC_S_CTRL failed");
    return GST_FLOW_ERROR;
  }

  /* Crop if needed */
  if ((width != crop_width) || (height != crop_height)) {
    memset (&cropcap, 0, sizeof cropcap);
    cropcap.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

    if (v4l2_ioctl (trans->fd, VIDIOC_CROPCAP, &cropcap) < 0) {
      GST_ERROR_OBJECT (trans, "Output VIDIOC_CROPCAP failed");
      return GST_FLOW_ERROR;
    }

    memset (&crop, 0, sizeof crop);
    crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    crop.c.width = crop_width;
    crop.c.height = crop_height;

    if (v4l2_ioctl (trans->fd, VIDIOC_S_CROP, &crop) < 0) {
      GST_ERROR_OBJECT (trans, "Output VIDIOC_S_CROP failed");
      return GST_FLOW_ERROR;
    }
  }

  /* Set number of buffers */
  memset (&reqbuf, 0, sizeof reqbuf);
  reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  reqbuf.count = NB_BUF_NO_OUTPUT_POOL;
  reqbuf.memory = V4L2_MEMORY_MMAP;

  if (v4l2_ioctl (trans->fd, VIDIOC_REQBUFS, &reqbuf) < 0) {
    GST_ERROR_OBJECT (trans, "Output VIDIOC_REQBUFS failed");
    return GST_FLOW_ERROR;
  }

  trans->out_no_pool.mmap_virt = malloc (sizeof (void *) * reqbuf.count);
  trans->out_no_pool.mmap_size = malloc (sizeof (void *) * reqbuf.count);

  memset (&querybuf, 0, sizeof querybuf);
  querybuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  querybuf.memory = V4L2_MEMORY_MMAP;

  for (i = 0; i < reqbuf.count; i++) {
    /* Get buffers */
    querybuf.index = i;
    trans->out_no_pool.current_nb_buf++;

    if (v4l2_ioctl (trans->fd, VIDIOC_QUERYBUF, &querybuf) < 0) {
      GST_ERROR_OBJECT (trans, "Output VIDIOC_QUERYBUF failed");
      return GST_FLOW_ERROR;
    }

    ptr = v4l2_mmap (NULL, querybuf.length, PROT_READ | PROT_WRITE,
        MAP_SHARED, trans->fd, querybuf.m.offset);

    if (ptr == MAP_FAILED) {
      GST_ERROR_OBJECT (trans, "Output MMAP FAILED");
      return GST_FLOW_ERROR;
    }

    trans->out_no_pool.mmap_virt[i] = ptr;
    trans->out_no_pool.mmap_size[i] = querybuf.length;

    /* Queue empty buffers */
    qbuf = querybuf;
    qbuf.bytesused = 0;
    if (v4l2_ioctl (trans->fd, VIDIOC_QBUF, &qbuf) < 0) {
      GST_ERROR_OBJECT (trans, "Output QBUF FAILED");
      return GST_FLOW_ERROR;
    }
    GST_DEBUG_OBJECT (trans, "Enqueued output buffer index %d", qbuf.index);
  }

  /* Start stream */
  type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  if (v4l2_ioctl (trans->fd, VIDIOC_STREAMON, &type) < 0) {
    GST_ERROR_OBJECT (trans, "Output STREAMON FAILED");
    return GST_FLOW_ERROR;
  }

  GST_DEBUG_OBJECT (trans, "Output ready : %dx%d", width, height);
  trans->out_no_pool.setup = TRUE;

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_v4l2trans_capture_start (GstV4L2Trans * trans, GstVideoFrame * out)
{
  gint type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  /* Start stream */
  if (v4l2_ioctl (trans->fd, VIDIOC_STREAMON, &type) < 0) {
    GST_ERROR_OBJECT (trans, "Capture STREAMON FAILED");
    return GST_FLOW_ERROR;
  }

  GST_DEBUG_OBJECT (trans, "Capture started : %dx%d",
      gst_v4l2trans_align_width (&out->info),
      gst_v4l2trans_align_height (&out->info));

  trans->capture_start = TRUE;
  return GST_FLOW_OK;
}

/* Return the v4l2 index corresponding to a fd.
 * First search if a fd has a corresponding v4l2 index
 * If not, add an element to the table, possibly overwriting an existing
 * element if table full
 */
static gint
gst_v4l2trans_get_capture_index (GstV4L2Trans * trans, gint fd)
{
  gint i, free = -1;

  for (i = 0; i < ARRAY_SIZE (trans->capture_v4l2_fd); i++) {
    if (trans->capture_v4l2_fd[i] == fd)
      return i;

    if ((free < 0) && (trans->capture_v4l2_fd[i] == -1))
      free = i;
  }

  /* Not found, add to the table */
  if (free < 0) {
    GST_WARNING_OBJECT (trans,
        "Capture_v4l2_fd table full: will overwrite for fd=%d", fd);
    free = 0;
  }

  trans->capture_v4l2_fd[free] = fd;
  return free;
}

static GstFlowReturn
gst_v4l2trans_output_no_pool_process (GstV4L2Trans * trans, GstVideoFrame * in)
{
  GstFlowReturn ret;
  GstBuffer *buf;
  struct v4l2_buffer out_buf;
  GstMapInfo mapinfo;
  guint8 *gstdata;
  gsize gstsize;

  /* Not using our buffer pool. Will use memcpy */
  if (!trans->out_no_pool.setup) {
    ret = gst_v4l2trans_setup_output_no_pool (trans, in);
    if (ret != GST_FLOW_OK) {
      GST_ERROR_OBJECT (trans, "Cannot setup output");
      return ret;
    }
  }

  /* Output: dequeue buffer */
  memset (&out_buf, 0, sizeof out_buf);
  out_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  out_buf.memory = V4L2_MEMORY_MMAP;
  if (v4l2_ioctl (trans->fd, VIDIOC_DQBUF, &out_buf) < 0) {
    GST_ERROR_OBJECT (trans, "Output DQBUF FAILED");
    return GST_FLOW_ERROR;
  }
  GST_DEBUG_OBJECT (trans, "Dequeued output buffer index %d", out_buf.index);

  /* Output: copy GST buffer into V4L2 buffer */
  buf = in->buffer;

  gst_buffer_map (buf, &mapinfo, GST_MAP_READ);
  gstdata = mapinfo.data;
  gstsize = mapinfo.size;
  memcpy (trans->out_no_pool.mmap_virt[out_buf.index], gstdata, gstsize);
  gst_buffer_unmap (buf, &mapinfo);

  /* Output: queue buffer */
  out_buf.bytesused = gst_buffer_get_size (buf);

  if (v4l2_ioctl (trans->fd, VIDIOC_QBUF, &out_buf) < 0) {
    GST_ERROR_OBJECT (trans, "Output QBUF FAILED");
    return GST_FLOW_ERROR;
  }
  GST_DEBUG_OBJECT (trans, "Enqueued output buffer %p, index %d",
      buf, out_buf.index);

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_v4l2trans_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * in, GstVideoFrame * out)
{
  GstV4L2Trans *trans = GST_V4L2TRANS (filter);
  GstBuffer *buf;
  GstFlowReturn ret;
  GstMemory *gmem;
  struct v4l2_buffer cap_buf;
  gint fd;
  gboolean from_our_pool = FALSE;
  guint8 *gstdata;
  gsize gstsize;
  void *ptr;

  GST_DEBUG_OBJECT (trans, "Transforming");

  /* Output: queue buffer */
  if (in->buffer->pool != trans->output_pool) {
    /* Not using our output buffer pool : will use memcpy */
    GST_DEBUG_OBJECT (trans, "SLOW path");
    ret = gst_v4l2trans_output_no_pool_process (trans, in);
    if (ret != GST_FLOW_OK) {
      GST_ERROR_OBJECT (trans, "Failed to process output buffer");
      return ret;
    }
  } else {
    /* Using our output buffer pool: will use dmabuf without copy */
    GST_DEBUG_OBJECT (trans, "FAST path");
    ret = gst_v4l2_out_buf_pool_process (trans->output_pool, in->buffer);
    if (ret != GST_FLOW_OK) {
      GST_ERROR_OBJECT (trans, "Queuing output frame failed");
      return ret;
    }
  }

  /* Capture: start (if not done yet) */
  if (!trans->capture_start) {
    ret = gst_v4l2trans_capture_start (trans, out);
    if (ret != GST_FLOW_OK)
      return ret;
  }

  /* Capture: get buffer properties */
  buf = out->buffer;
  gmem = gst_buffer_get_memory (buf, 0);
  if (gst_is_dmabuf_memory (gmem)) {
    /* DMABUF: use fd */
    fd = gst_dmabuf_memory_get_fd (gmem);

    /* Check buffer pool of the capture buffer */
    if (buf->pool == trans->capture_pool)
      from_our_pool = TRUE;
    else if (buf->pool == trans->capture_down_pool)
      from_our_pool = FALSE;
    else {
      GST_ERROR_OBJECT (trans, "Capture buffer %p from unknown pool %p %s",
          buf, buf->pool, buf->pool->object.name);

      gst_memory_unref (gmem);
      return GST_FLOW_ERROR;
    }
  } else {
    GST_DEBUG_OBJECT (trans, "The capture buffer %p is not a DMABUF one,"
        "will use SLOW path", buf);
    fd = -1;
  }
  gst_memory_unref (gmem);

  /* Capture: queue buffer */
  memset (&cap_buf, 0, sizeof cap_buf);
  if ((from_our_pool) || (fd == -1))
    cap_buf.memory = V4L2_MEMORY_MMAP;
  else
    cap_buf.memory = V4L2_MEMORY_DMABUF;

  cap_buf.m.fd = fd;
  cap_buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  cap_buf.bytesused = gst_buffer_get_size (out->buffer);
  cap_buf.length = cap_buf.bytesused;
  if (fd == -1) {
    /* We can use a unique index as no DMABUF optimization possible here */
    cap_buf.index = 0;
  } else {
    /* We have to keep m.fd <-> index consistency to get V4L2 DMABUF working */
    cap_buf.index = gst_v4l2trans_get_capture_index (trans, fd);
  }

  if (v4l2_ioctl (trans->fd, VIDIOC_QBUF, &cap_buf) < 0) {
    GST_ERROR_OBJECT (trans, "Capture QBUF FAILED");
    return GST_FLOW_ERROR;
  }
  GST_DEBUG_OBJECT (trans, "Enqueued capture buffer %p fd= %d index=%d", buf,
      cap_buf.m.fd, cap_buf.index);

  /* Capture: dequeue the processed buffer. This call blocks until the HW
   * transformation is actually done */
  if (v4l2_ioctl (trans->fd, VIDIOC_DQBUF, &cap_buf) < 0) {
    GST_ERROR_OBJECT (trans, "Capture DQBUF FAILED");
    return GST_FLOW_ERROR;
  }
  GST_DEBUG_OBJECT (trans, "Dequeued capture buffer %p fd=%d index=%d", buf,
      cap_buf.m.fd, cap_buf.index);

  /* Capture non-DMABUF: map and copy V4L2 buffer into GST buffer */
  if (fd == -1) {
    if (v4l2_ioctl (trans->fd, VIDIOC_QUERYBUF, &cap_buf) < 0) {
      GST_ERROR_OBJECT (trans, "Output VIDIOC_QUERYBUF failed");
      return GST_FLOW_ERROR;
    }

    ptr = v4l2_mmap (NULL, cap_buf.length, PROT_READ | PROT_WRITE,
        MAP_SHARED, trans->fd, cap_buf.m.offset);
    if (ptr == MAP_FAILED) {
      GST_ERROR_OBJECT (trans, "Capture MMAP FAILED");
      return GST_FLOW_ERROR;
    }

    gstdata = GST_VIDEO_FRAME_PLANE_DATA (out, 0);
    gstsize = GST_VIDEO_FRAME_SIZE (out);
    memcpy (gstdata, ptr, gstsize);

    v4l2_munmap (ptr, cap_buf.length);
    GST_DEBUG_OBJECT (trans, "Buffer copied");
  }

  GST_DEBUG_OBJECT (trans, "Transforming done");
  return GST_FLOW_OK;
}

void
gst_v4l2trans_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstV4L2Trans *trans = GST_V4L2TRANS (object);
  GST_DEBUG_OBJECT (trans, "Setting property");

  switch (prop_id) {
    case PROP_HFLIP:
      trans->hflip = g_value_get_boolean (value);
      break;
    case PROP_VFLIP:
      trans->vflip = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

void
gst_v4l2trans_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstV4L2Trans *trans = GST_V4L2TRANS (object);
  GST_DEBUG_OBJECT (trans, "Getting property");

  switch (prop_id) {
    case PROP_HFLIP:
      g_value_set_boolean (value, trans->hflip);
      break;
    case PROP_VFLIP:
      g_value_set_boolean (value, trans->vflip);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstStateChangeReturn
gst_v4l2trans_change_state (GstElement * element, GstStateChange transition)
{
  GstV4L2Trans *trans = GST_V4L2TRANS (element);
  GstStateChangeReturn ret;

  if (transition == GST_STATE_CHANGE_NULL_TO_READY)
    if (!gst_v4l2trans_open_device (trans))
      return GST_STATE_CHANGE_FAILURE;

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  if (transition == GST_STATE_CHANGE_READY_TO_NULL)
    gst_v4l2trans_close_device (trans);

  return ret;
}

static void
gst_v4l2trans_init (GstV4L2Trans * trans)
{
}

static void
gst_v4l2trans_class_init (GstV4L2TransClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstBaseTransformClass *gstbasetransform_class;
  GstVideoFilterClass *gstvideofilter_class;
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gstbasetransform_class = (GstBaseTransformClass *) klass;
  gstvideofilter_class = (GstVideoFilterClass *) klass;


  gst_element_class_add_pad_template (element_class,
      gst_v4l2trans_sink_template_factory ());
  gst_element_class_add_pad_template (element_class,
      gst_v4l2trans_src_template_factory ());

  gstbasetransform_class->stop = GST_DEBUG_FUNCPTR (gst_v4l2trans_stop);
  gstbasetransform_class->decide_allocation =
      GST_DEBUG_FUNCPTR (gst_v4l2trans_decide_allocation);
  gstbasetransform_class->propose_allocation =
      GST_DEBUG_FUNCPTR (gst_v4l2trans_propose_allocation);
  gstbasetransform_class->transform_caps =
      GST_DEBUG_FUNCPTR (gst_v4l2trans_transform_caps);
  gstbasetransform_class->fixate_caps =
      GST_DEBUG_FUNCPTR (gst_v4l2trans_fixate_caps);
  gstbasetransform_class->passthrough_on_same_caps = TRUE;

  gstvideofilter_class->transform_frame =
      GST_DEBUG_FUNCPTR (gst_v4l2trans_transform_frame);

  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_v4l2trans_finalize);
  gobject_class->set_property = GST_DEBUG_FUNCPTR (gst_v4l2trans_set_property);
  gobject_class->get_property = GST_DEBUG_FUNCPTR (gst_v4l2trans_get_property);

  element_class->change_state = GST_DEBUG_FUNCPTR (gst_v4l2trans_change_state);

  g_object_class_install_property (gobject_class, PROP_HFLIP,
      g_param_spec_boolean ("hflip", "hflip", "Horizontal flip", 0,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_VFLIP,
      g_param_spec_boolean ("vflip", "vflip", "Vertical flip", 0,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  GST_DEBUG_CATEGORY_INIT (gst_v4l2trans_debug, "v4l2trans", 0,
      "ST V4L2 transformer");

  gst_element_class_set_static_metadata (element_class,
      "V4L2 transform", "Video/Transform",
      "A V4L2 transformer", "STMicroelectronics");
}

gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "v4l2trans", GST_RANK_PRIMARY + 1,
          GST_TYPE_V4L2TRANS))
    return FALSE;
  return TRUE;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    v4l2trans,
    "ST V4L2 transform",
    plugin_init, VERSION, GST_LICENSE, GST_PACKAGE_NAME, GST_PACKAGE_ORIGIN);
