/*
* Copyright (C) 2015 STMicroelectronics SA
*
* Author: <fabien.dessenne@st.com> for STMicroelectronics.
*
*/
#ifndef __GST_V4L2_CAP_BUF_POOL_H__
#define __GST_V4L2_CAP_BUF_POOL_H__

#include <gst/gst.h>
#include <gst/allocators/gstdmabuf.h>

typedef struct _GstV4L2CapBufPool GstV4L2CapBufPool;
typedef struct _GstV4L2CapBufPoolClass GstV4L2CapBufPoolClass;

#include "gstv4l2trans.h"

G_BEGIN_DECLS
#define GST_TYPE_V4L2_CAP_BUF_POOL     (gst_v4l2_cap_buf_pool_get_type())
#define GST_IS_V4L2_CAP_BUF_POOL(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_TYPE_V4L2_CAP_BUF_POOL))
#define GST_V4L2_CAP_BUF_POOL(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_TYPE_V4L2_CAP_BUF_POOL, \
    GstV4L2CapBufPool))
#define GST_V4L2_CAP_BUF_POOL_CAST(obj) ((GstV4L2CapBufPool*)(obj))

struct _GstV4L2CapBufPool
{
  GstBufferPool parent;
  GstV4L2Trans *trans;
  gint v4l2_fd;
  guint num_allocated;
};

struct _GstV4L2CapBufPoolClass
{
  GstBufferPoolClass parent_class;
};

GType gst_v4l2_cap_buf_pool_get_type (void);
GstBufferPool *gst_v4l2_cap_buf_pool_new (GstV4L2Trans * trans,
    GstCaps * caps, gsize size, guint max);
G_END_DECLS
#endif /*__GST_V4L2_CAP_BUF_POOL_H__ */
